---
title: Kanu-Polo
weight: 20
chapter: true
pre: "<b>2. </b>"
date                    : 2019-12-16T19:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

### Kapitel 2

# Kanu-Polo

![Ball](Ball.jpg)

Hier steht alles, was man über Kanu-Polo wissen muss.

{{% children depth="1" style="li" showhidden="true" %}}
