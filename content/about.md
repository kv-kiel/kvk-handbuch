---
title                   : "Über uns..."
tags                    : []
ressorts                : []
date                    : 2020-01-01T17:22:55+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

![Wimpel](../Wimpel.jpg)

Die
[Kanu-Vereinigung Kiel e. V.](https://kv-kiel.de)
hat ihren Sitz in Kiel, Düsternbrooker Weg 44. Sie ist in das Vereinsregister beim Amtsgericht in Kiel eingetragen.

[Impressum](Impressum/)