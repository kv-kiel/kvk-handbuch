---
title   : "Zu dieser Webseite beitragen"
date                    : 2019-12-16T19:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

Diese Webseite ist mit
[Hugo](https://gohugo.io/)
erstellt und wird auf
[Gitlab](https://gitlab.com/kv-kiel/kvk-handbuch/)
gehostet.

Das Theme, auf dem die Seite basiert, ist
[hugo-theme-learn](https://themes.gohugo.io/theme/hugo-theme-learn/).
