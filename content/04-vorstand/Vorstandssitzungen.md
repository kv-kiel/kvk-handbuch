---
title 	: "Vorstandssitzungen"
tags	: [Veranstaltungen]
ressorts: []
weight	: 30
date                    : 2020-01-24T12:47:52+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

Vorstandssitzungen finden grob einmal im Monat statt und sind vereinsöffentlich.

Die 1. Vorsitzende lädt rechtzeitig vorher zur Vorstandssitzung ein - im Allgemeinen per eMail.
Dabei wird die vorläufige Tagesordnung mitgeschickt.

Wir freuen uns immer, wenn neu eingetretene Mitglieder sich auf einer Vorstandssitzung
einmal vorstellen. So ist ein gegenseitiges Kennenlernen möglich, und man muss
sich bei einer Begegnung in der Bootshalle nicht fragen, wen man da eigentlich
vor sich hat.
