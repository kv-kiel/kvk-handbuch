---
title: Vorstand
chapter: true
pre: "<b>4. </b>"
weight: 40
date                    : 2016-04-09T16:50:16+02:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

### Kapitel 4

# Vorstand

Hier steht alles, was man über den Vorstand wissen muss.

![Organigramm](Organigramm.png)

{{% children depth="1" style="li" showhidden="true" %}}
