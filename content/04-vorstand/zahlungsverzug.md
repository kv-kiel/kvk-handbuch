---
title                   : "Zahlungsverzug"
tags                    : []
ressorts                : [ "1. Vorsitzende", Mitgliederwartin, Kassenwartin ]
date                    : 2020-07-14T21:09:56+02:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
weight	                : 100
---

<!-- TOC -->

- [1. Feststellung von Zahlungsverzügen](#1-feststellung-von-zahlungsverzügen)
- [2. Verfolgung von Zahlungsverzügen](#2-verfolgung-von-zahlungsverzügen)
  - [2.1. Feststellung der Gründe](#21-feststellung-der-gründe)
  - [2.2. Einbindung des Mitgliedes zur Klärung/Problemlösung](#22-einbindung-des-mitgliedes-zur-klärungproblemlösung)
  - [2.3. Schriftliche Mahnung](#23-schriftliche-mahnung)
  - [2.4. Gerichtliches Mahnverfahren](#24-gerichtliches-mahnverfahren)

<!-- /TOC -->

## 1. Feststellung von Zahlungsverzügen

Dass ein Mitglied in Zahlungsverzug kommt, kommt immer wieder vor und kann sehr unterschiedliche Gründe haben.
Das ist kein großes Drama, verursacht aber auch im harmlosesten Fall eine Menge Arbeit.
Daher hat vor allem die Mitgliederwartin die große Bitte an alle Mitglieder, ihre Konto- (und auch die Adress-) Daten bei Änderung möglichst umgehend zu melden.

Die Durchführung eines SEPA-Einzugslauf durch die Mitgliederwartin hat zur Folge, dass jedes Mitgliedskonto mit dem individuellen Beitrags belastet wird - und dass ihm unmittelbar danach derselbe Beitrag gutgeschrieben wird, weil das Programm davon ausgeht, dass die Abbuchung auch erfolgreich durchgeführt wurde.
Aus Sicht der Mitgliederwartin ist das Mitgliederkonto also ausgeglichen.

Ein paar Tage nach dem Fälligkeitstermin der Abbuchung kann die Kassenwartin über einen Einblick in die Kontobewegungen feststellen, ob SEPA-Einzüge zurückgewiesen worden sind.
Diese Feststellung muss sie manuell an die Mitgliederwartin schicken.

Die Mitgliederwartin muss dann für die Mitglieder, bei denen es eine Rückweisung gegeben hat, im Mitgliederkonto einen entsprechenden Betrag als Soll einbuchen.
Erst damit ist der Saldo des Mitgliederkontos negativ.

## 2. Verfolgung von Zahlungsverzügen

Der geschäftsführende Vorstand ist gesetzlich dazu verpflichtet, Zahlungsrückstände einzutreiben.

Sollte also einmal der Fall eingetreten sein, dass eine fällige Abbuchung nicht erfolgreich durchgeführt werden konnte und damit das Mitgliedskonto einen negativen Saldo aufweist, soll nach folgendem Schema vorgegangen werden, um die Lok wieder aufs Gleis zu heben:

1. Feststellung der Gründe
2. Einbindung des Mitgliedes zur Klärung/Problemlösung
3. Schriftliche Mahnung
4. Weitere Mahnung mit Androhung des Ausschlusses
5. Gerichtliches Mahnverfahren

Es gibt also drei Schreiben an das Mitglied, bevor das gerichtliche Mahnverfahren eröffnet wird:
1. Mitteilung und Erinnerung, dass Zahlungsrückstand besteht und in welcher Höhe
2. Erste Mahnung
3. Zweite Mahnung mit Androhung des Ausschlusses und der Eröffnung eines gerichtlichen Mahnverfahrens

Nach jedem dieser drei Schreiben wird dem Mitglied eine Zahlungsfrist von zwei Wochen eingeräumt, um den Saldo des Mitgliedskontos auszugleichen.
Zusammen mit den Verzögerungen, die entstehen, weil die Kassenwartin erst die Kontoauszüge überprüfen und die Mitgliederwartin das nächste Schreiben erstellen muss, können leicht vier Wochen zwischen den Schreiben liegen. Der gesamte Prozess sollte nach Möglichkeit abgeschlossen sein, bevor der nächste SEPA-Lauf erfolgt (der in der Regel nach drei Monaten stattfindet).

Die Außenstände müssen jeweils durch das Mitglied manuell überwiesen werden. Wir können nicht die Möglichkeit nutzen, die fehlenden Beträge mit dem nächsten SEPA-Lauf einzuziehen, weil das den Prozess indiskutabel in die Länge ziehen würde.

Es ist darauf zu achten, dass die Aktivitäten, die zur Eintreibung von Außenständen vorgenommen wurden, dokumentiert werden, damit im Falle nicht eintreibbarer Außenständen dem Vorstand kein Versäumnis unterstellt werden kann.

### 2.1. Feststellung der Gründe

Es kann sehr unterschiedliche Gründe geben, warum ein Mitglied in Zahlungsverzug gerät. Die häufigste Urschache ist vermutlich eine geänderte Bankverbindung. Aber z.B. auch nicht ausreichend vorhandenes Guthaben kann ein Grund sein, warum ein SEPA-Einzug abgewiesen wird.

In dieser Phase geht es lediglich darum, sachlich festzustellen, was tatsächlich die Gründe für den Ausfall sind und, wenn nötig und möglich, Hilfe anzubieten. 
So kann ein nicht hinreichendes Guthaben eine Lapalie und unglücklicher Zufall sein, es kann aber auch einen Hintergrund haben, vor dem die Beitragszahlung an den Verein als Lapalie erscheint.

Daher erfolgt das Vorgehen in dieser Phase selbstverständlich unter größter Diskretion.
In erkennbar schwierigen Fällen ist unbedingt der geschäftsführende Vorstand einzubinden.

### 2.2. Einbindung des Mitgliedes zur Klärung/Problemlösung

In dieser Phase soll dem Mitglied Gelegenheit gegeben werden, entweder den Sachverhalt weiter aufzuklären oder das Problem gleich zu beseitigen.

Im Falle eines SEPA-Rücklaufes wegen nicht korrekter Bankverbindung wird eine Mail verschickt, die dazu auffordert, eine korrekte Bankverbindung anzugeben.

Im Falle eines SEPA-Rücklaufes wegen ungenügender Deckung kann ebenfalls eine entsprechende Mail verschickt werden.
Im Falle, dass eine prekäre Situation des Mitgliedes vermutet werden kann, sollte besser ein persönliches Gespräch bzw. ein Telefonat angestrebt werden.
In jedem Fall ist ein Termin zu vereinbaren, bis wann eine Lösung angestrebt wird.

Im Falle, dass einem SEPA-Einzug widersprochen worden ist, muss versucht werden, die Gründe direkt vom Mitglied zu erfragen - über ein persönliches Gespräch, eine Mail oder ein Telefonat.
Auch hier ist ein spätester Termin für eine Antwort zu vereinbaren.

### 2.3. Schriftliche Mahnung

Ist nach einer angemessenen Wartezeit nach dem in Phase 2 vereinbarten Termin noch keine Reaktion erfolgt, wird jetzt die erste offizielle Mahnung verschickt.
Dafür wird ein vorbereitetes Standardschreiben verwendet.

Ist der in der ersten Mahnung genannte Termin ohne Wirkung verstrichen, wird eine zweite Mahnung verschickt.
Auch hierfür wird ein vorbereitetes Standardschreiben verwendet.

In der zweiten Mahnung - also im dritten Schreiben - werden dann ein gerichtliches Mahnverfahren und der Vereinsausschluss angedroht.

### 2.4. Gerichtliches Mahnverfahren

Beim Mahnverfahren beantragt man beim Gericht einen Mahnbescheid und danach einen Vollstreckungsbescheid. Das ist bei weitem günstiger als eine Klageerhebung.

Um einen Mahnbescheid zu erwirken, kann man den [Antrag online](http://www.online-mahnantrag.de) stellen. 

Als letzten Versuch kann man aber auch ein Formular im Handel kaufen, ausfüllen und eine Kopie an das Mitglied schicken. An die Kopie befestigt man eine Haftnotiz mit dem Hinweis, dass der in Kopie beigefügte Antrag am … abgesandt wird. Ob allerdings so eine „letzte Chance“ sinnvoll ist, muss man im Einzelfall entscheiden.

Das Mahnverfahren ist schneller als eine Klage, da es ohne Gerichtsverhandlung abgewickelt wird. Nach Erhalt des Mahnbescheids kann der Empfänger binnen zwei Wochen Widerspruch einlegen. Tut er dies, kommt es zur Gerichtsverhandlung. Verzichtet er darauf, kann man nun einen Vollstreckungsbescheid beantragen, dem das Mitglied wieder innerhalb von 14 Tagen widersprechen kann.

Ob dieser letzte Schritt tatsächlich durchgeführt wird, entscheidet der Gesamtvorstand - ebenso über einen eventuellen Vereinsausschluss nach §3 der Satzung.
