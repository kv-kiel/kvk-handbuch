---
title                   : "Kalender"
tags                    : []
ressorts                : ["1. Vorsitzende","2. Vorsitzende",Bauausschuss,Bootshallenwartin,Festausschuss,Jugendwartin,Kassenwartin,Mitgliederwartin,Pressewartin,Schriftwartin,Kanupolowartin,Wanderwartin]
date                    : 2021-05-04T21:47:39+02:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

Hier sind die wichtigsten Termine aufgelistet, die die unterschiedlichen Referenten übers Jahr wahrnehmen müssen.

----
## Januar

- [**1.Vorsitzende:**]({{< relref "04-vorstand/ressorts/1.Vorsitzende.md" >}})
  - Mitgliederversammlung vorbereiten
  - Ggfs. zu Nachbarschaftstreffen einladen
- [**Kassenwartin:**]({{< relref "04-vorstand/ressorts/Kassenwartin.md" >}}) 
  - Etatplanung für Mitgliederversammlung abschließen
  - Kassenprüfung durchführen lassen
  - Nebenkostenabrechnung erstellen

----
## Februar

- [**Jugendwartin:**]({{< relref "04-vorstand/ressorts/Jugendwartin.md" >}}) Jugendversammlung durchführen

----
## März

- [**1.Vorsitzende:**]({{< relref "04-vorstand/ressorts/1.Vorsitzende.md" >}}) 
  - Mitgliederversammlung durchführen
  - Ggfs. Nachbarschaftstreffen durchführen
- [**Mitgliederwartin:**]({{< relref "04-vorstand/ressorts/Mitgliederwartin.md" >}})
  - Anpassung der Daten der aus dem Amt ausgeschiedenen sowie der neu gewählten Vorstandsmitglieder
  - [Überprüfung der Beitragsarten](https://kv-kiel.gitlab.io/kvk-doku-center/kvk-handbuch-spg-verein/anwendungsfaelle.html#_%C3%BCberpr%C3%BCfung_der_beitragsarten)
  - Beiträge für 1. Halbjahr einziehen

----
## April

- [**Wanderwartin:**]({{< relref "04-vorstand/ressorts/Wanderwartin.md" >}}) Anpaddeln durchführen

- [**Kassenwartin:**]({{< relref "04-vorstand/ressorts/Kassenwartin.md" >}}) Übungsleiterzuschüsse beantragen: Liste zusammenstellen und Antrag abschicken

----
## Mai



----
## Juni

- [**Kanupolowartin:**]({{< relref "04-vorstand/ressorts/kanupolowartin.md" >}}) Kieler-Woche-Turnier durchführen
- [**Bauausschuss:**]({{< relref "04-vorstand/ressorts/Bauausschuss.md" >}}) Arbeitsdienstabrechnung für das erste Halbjahr des Jahres abschließen
- [**Mitgliederwartin:**]({{< relref "04-vorstand/ressorts/Mitgliederwartin.md" >}}) Ersatzzahlungen für Arbeitsdienste des ersten Halbjahres einziehen

----
## Juli


----
## August

----
## September

- [**Wanderwartin:**]({{< relref "04-vorstand/ressorts/Wanderwartin.md" >}}) Erstellung des Winterprogrammes initiieren.

- [**Mitgliederwartin:**]({{< relref "04-vorstand/ressorts/Mitgliederwartin.md" >}})
  - [Überprüfung der Beitragsarten](https://kv-kiel.gitlab.io/kvk-doku-center/kvk-handbuch-spg-verein/anwendungsfaelle.html#_%C3%BCberpr%C3%BCfung_der_beitragsarten)
  -  Mitgliedsbeitrag zweites Halbjahr einziehen


----
## Oktober

- [**1.Vorsitzende:**]({{< relref "04-vorstand/ressorts/1.Vorsitzende.md" >}}) Beginn der Etatplanung, Fachreferenten auffordern, ihre Planung abzugeben.

- [**Wanderwartin:**]({{< relref "04-vorstand/ressorts/Wanderwartin.md" >}})
  - Auffordern zur Abgabe der persönlichen Fahrtenbücher
  - Auswertung des Vereinsfahrtenbuchs und Einschicken der Ergebnisse
  - Abpaddeln durchführen

----
## November

- [**1.Vorsitzende:**]({{< relref "04-vorstand/ressorts/1.Vorsitzende.md" >}}) Termin für Mitgliederversammlung festlegen

- [**Bauausschuss:**]({{< relref "04-vorstand/ressorts/Bauausschuss.md" >}}) Arbeitsdienstabrechnung für das zweite Halbjahr desselben Jahres abschließen

----
## Dezember

- [**1.Vorsitzende:**]({{< relref "04-vorstand/ressorts/1.Vorsitzende.md" >}}) Einladung für Mitgliederversammlung fertigstellen und Verschickung oder Veröffentlichung in der _Platsch_ sicherstellen.

- [**Jugendwartin:**]({{< relref "04-vorstand/ressorts/Jugendwartin.md" >}}) Termin für Jugendversammlung festlegen und Einladungen verschicken

- [**Mitgliederwartin:**]({{< relref "04-vorstand/ressorts/Mitgliederwartin.md" >}})
  - Ersatzzahlungen für Arbeitsdienste zweites Halbjahr einziehen
  - Jahresabschluss Mitgliederverwaltung durchführen
  - Stärkemeldungen an die Verbände abgeben (LSV, LKV, SV-Kiel(?))
  - Aufruf zur Einreichung der Nachweise für Betragsermäßigung (z.B. durch Email-Rundschreiben oder kurzen Artikel in der Vereinszeitung)

- [**Kassenwartin:**]({{< relref "04-vorstand/ressorts/Kassenwartin.md" >}})
  - Jahresabschluss durchführen

