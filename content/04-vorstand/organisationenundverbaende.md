---
title                   : "Organisationen und Verbände"
tags                    : []
ressorts                : ["1. Vorsitzende", "2. Vorsitzende"]
date                    : 2020-04-21T22:31:04+02:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

<!-- TOC -->

- [1. Sportverband Kiel](#1-sportverband-kiel)
- [2. Fachgruppe Kanu im Sportverband Kiel](#2-fachgruppe-kanu-im-sportverband-kiel)
- [3. Landessportverband Schleswig-Holstein](#3-landessportverband-schleswig-holstein)
- [4. Landeskanuverband Schleswig-Holstein](#4-landeskanuverband-schleswig-holstein)
- [5. Deutscher Kanu-Verband](#5-deutscher-kanu-verband)
- [6. Nachbarvereine](#6-nachbarvereine)
- [7. Kieler Kanu-Klub](#7-kieler-kanu-klub)

<!-- /TOC -->

Wie jeder Verein ist auch die Kanu-Vereinigung Kiel in ein Netzwerk von anderen Organisationen eingebunden.
Das sind zum, einen klassische und formale Verbandsbeziehungen und zum anderen einfach informelle Beziehungen zu anderen Vereinen.

<a id="markdown-1-sportverband-kiel" name="1-sportverband-kiel"></a>
## 1. Sportverband Kiel

Alle Sportvereine innerhalb des Kieler Stadtgebietes sind im Sportverband Kiel organisiert. Das entspricht den Kreissportverbänden der Landkreise - da Kiel aber eine kreisfreie Stadt ist, muss die Vorsilbe "Kreis" halt entfallen :-|

<a id="markdown-2-fachgruppe-kanu-im-sportverband-kiel" name="2-fachgruppe-kanu-im-sportverband-kiel"></a>
## 2. Fachgruppe Kanu im Sportverband Kiel

Alle Kanu-Vereine innerhalb des Kieler Stadtgebietes sind in der Fachgruppe Kanu organisiert. Dieser Kreis trifft sich einmal im Jahr, wo man sich über anliegende Themen austauschen kann.

<a id="markdown-3-landessportverband-schleswig-holstein" name="3-landessportverband-schleswig-holstein"></a>
## 3. Landessportverband Schleswig-Holstein

Alle Sportvereine des Landes Schleswig-Holstein sind im Landessportverband organisiert.

<a id="markdown-4-landeskanuverband-schleswig-holstein" name="4-landeskanuverband-schleswig-holstein"></a>
## 4. Landeskanuverband Schleswig-Holstein

Alle Kanu-Vereine des Landes Schleswig-Holstein sind im Landeskanuverband Schleswig-Holstein organisiert.

<a id="markdown-5-deutscher-kanu-verband" name="5-deutscher-kanu-verband"></a>
## 5. Deutscher Kanu-Verband

Alle Kanu-Vereine Deutschlands sind im Deutschen Kanu-Verband organisiert.

<a id="markdown-6-nachbarvereine" name="6-nachbarvereine"></a>
## 6. Nachbarvereine

Mit den in unmittelbarer Nachbarschaft liegenden Vereinen pflegen wir eine freundschaftliche Beziehung. Die Vereine sind im Einzelnen:

| Name                       | Kontakt |
| -------------------------- | ------- |
| Kieler Kanu-Klub           |         |
| Rudergesellschaft Germania |         |
| Marine Kameradschaft Kiel  |         |
| Marine-Jugend Kiel         |         |

Einmal im Jahr treffen sich diese Vereine in gemütlicher Runde, um sich über den aktuellen Stand zu informieren und ggfs. gemeinsam interessierende Themen miteinander zu besprechen.

In der Regel nehmen die ersten und ggfs. zweiten Vorsitzenden an dieser Runde teil.
Der das Treffen ausrichtende Verein bietet bei diesem Anlass auch immer eine kleine "Vesper" an.

<a id="markdown-7-kieler-kanu-klub" name="7-kieler-kanu-klub"></a>
## 7. Kieler Kanu-Klub

Mit unserem direkten Nachbarverein pflegen wir eine besonders enge Beziehung. Es besteht ein Kooperationsabkommen, nachdem alle Veranstaltungen und Angebote des einen Vereins auch den Mitgliedern des jeweils anderen Vereins zur Verfügung stehen.
