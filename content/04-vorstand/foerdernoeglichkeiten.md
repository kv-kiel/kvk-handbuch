---
title   : "Fördermöglichkeiten"
weight	: 40
tags	: []
ressorts: [ "1. Vorsitzende", Wanderwartin, Kanupolowartin, Kassenwartin ]
date                    : 2020-02-16T16:13:09+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

<!-- TOC -->

- [1. Einleitung](#1-einleitung)
- [2. Übungsleiter/Trainer](#2-%c3%9cbungsleitertrainer)
- [3. Material und Ausrüstung](#3-material-und-ausr%c3%bcstung)
- [4. Sportstättenförderung](#4-sportst%c3%a4ttenf%c3%b6rderung)

<!-- /TOC -->

<a id="markdown-1-einleitung" name="1-einleitung"></a>
## 1. Einleitung

Es gibt eine Reihe von Fördermöglichkeiten

<a id="markdown-2-übungsleitertrainer" name="2-übungsleitertrainer"></a>
## 2. Übungsleiter/Trainer

Auf Grundlage des §3 der Sportförderrichtlinie der Stadt stellt die Stadt Kiel den Vereinen über den Sportverband Kiel ein Gesamtbudget für Übungsleiter und Mitgliederzuschüsse zur Verfügung:

> 3 Übungsleiter- und Mitgliederzuschüsse
>
> 3.1 Im Rahmen der Haushaltsberatungen kann die Ratsversammlung der Landeshauptstadt Kiel den Vereinen über den Sportverband Kiel ein Gesamtbudget für Übungsleiter und Mitgliederzuschüsse zur Verfügung stellen. Pro volle 40 Vereinsmitglieder kann ein Übungsleiter berücksichtigt werden, sofern eine anerkannte gültige Ausbildungslizenz vorliegt. Ebenfalls anerkannt werden examinierte Sportlehrer oder Sportstudenten mit Vorexamen. Darüber hinaus wird den Vereinen mit o.g. Übungsleitern ein Mitgliederzuschuss wir folgt berechnet: Pro jugendliches Vereinsmitglied bis 18 Jahre (vollendetes 19. Lebensjahr) bis zu 2 €, pro erwachsenes Mitglied bis zu 1 €. Der Restbetrag wird als Übungsleiterzuschuss, orientiert an der Anzahl der Übungsleiter, gewährt.
>
>3.2 Der Zuschuss ist jährlich von den Vereinen bis zum 30.06. des Jahres beim Sportverband Kiel zu beantragen. Für alle Übungsleiter/innen sind zum Nachweis ihrer Qualifikation gültige Lizenzen bzw. Examensbescheinigungen vorzulegen. Später eingehende Anträge können nicht berücksichtigt werden.

Vereine beantragen diese Zuschüsse über den Sportverband (werden auffordernd angeschrieben, Abgabetermin ist etwa Mai).

Verleibt von der zur Verfügung stehenden Fördersumme noch Geld, nachdem die Verteilung nach dem oben beschriebenen Schlüssel erfolgt ist, wird der Restbetrag als Übungsleiterzuschuss gewährt, orientiert an der Anzahl der Übungsleiter. Das bedeutet, dass erst nach Eingang aller Anträge aus den Vereinen festgelegt wird, welche Summe pro anerkanntem Übungsleiter ausgezahlt wird. Der Betrag lag in den vergangenen Jahren zwischen 100 und 200 Euro pro Übungsleiter.

Übungsleiter mt einer anerkannten gültigen Ausbildung sind solche mit mindestens einer C-Lizenz oder einer C-Lizenz-äquivalenten Ausbildung. Im Seekajak-Bereich wären das "Trainer-C Breitensport" und im Bereich Kanupolo "Trainer-C Leistungssport".

C-Lizenz-äquivalent wäre jede andere Lizenz mit mindestens 120 Std. Ausbildung. Es gibt etliche Sportarten, in denen es Ausbildungen gibt, die gleich- oder höherwertig denen des Trainer-C sind und die dann, wenn sie zugelassen werden, auch zuschussfähig sind.

Zitat aus einer Nachfrage:
>Wenn ich das richtig verstanden habe, prüft das Fr. Jacobsen selbst. Sie sagte, sie versucht, fast alles zu genehmigen, was irgendwie hinein passt. Man soll die ggf. äquivalente Ausbildung einfach bei ihr einreichen und Umfang und die Inhalte der Ausbildung dazu legen. Schlimmstenfalls sagt sie halt nein. Sie sagte auch, gewisse Pflichtinhalte wie Recht und Dopingprävention sollten schon dabei sein.

<a id="markdown-3-material-und-ausrüstung" name="3-material-und-ausrüstung"></a>
## 3. Material und Ausrüstung

<a id="markdown-4-sportstättenförderung" name="4-sportstättenförderung"></a>
## 4. Sportstättenförderung