---
title                   : "Kinderschutz"
tags                    : []
ressorts                : [Jugendwartin, "2. Vorsitzende"]
date                    : 2020-12-08T19:44:40+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

Unser Verein ist für Kinder und Jugendliche ein Hort für eine attraktive Freizeitgestaltung und soziales Miteinander. Die besondere Verletzlichkeit und Schutzbedürftigkeit dieser Altersgruppe ist für den Verein Verpflichtung, sich in besonderer Weise um das Wohl der ihm anvertrauten Minderjährigen zu kümmern.

Folgende Personen müssen ein erweitertes Führungszeugnis vorlegen:

- alle Übungsleitenden
- alle in der Jugendarbeit Tätigen
- alle neu in ein Vorstandsamt aufgenommenen Personen
   
Das Vorweisen des Führungszeugnisses wird dokumentiert. Es wird ohne Anfertigung einer Kopie zurückgegeben.

Neben der Vorlage des Führungszeugnisses ist die vereinseigene [Ehrenerklärung](https://kv-kiel.de/verein/downloads/Ehrenerklaerung.pdf) zu unterschreiben.

Allen oben genannten Personen sind folgende Dokumente auszuhändigen:

- [Verhaltensleitfaden im Sport- und Vereinsbetrieb](https://kv-kiel.de/verein/downloads/VerhaltensleitfadenImSport-UndVereinsbetrieb.pdf)
- [Handlungsleitfaden im Verdachtsfall](https://kv-kiel.de/verein/downloads/HandlungsleitfadenImVerdachtsfall.pdf)
