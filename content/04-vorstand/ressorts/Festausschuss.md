---
title   : "Festausschuss"
subtitle: "Stellenbeschreibung"
weight	: 120
ressorts: [ Festausschuss ]
date                    : 2022-08-21T10:24:10+02:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

## Grundlegendes

|  |  |
| --- | --- |
| Email-Adresse | festwart@kv-kiel.de  |
| wird vertreten von | [Festausschuss]({{< relref "04-vorstand/ressorts/Festausschuss.md" >}}) |
| vertritt |  [Festausschuss]]({{< relref "04-vorstand/ressorts/Festausschuss.md" >}}) |

## Geschäftsführungsaufgaben

1. Kauf von Lebensmitteln für vereinsinterne Veranstaltungen
2. Anwerbung und Verpflichtung von Musikergruppen für vereinsinterne Feste im Rahmen des Etats des Festausschusses
