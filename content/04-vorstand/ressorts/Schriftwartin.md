---
title   : "Schriftwartin"
subtitle: "Stellenbeschreibung"
weight	: 80
ressorts: [ Schriftwartin ]
date                    : 2020-01-24T15:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

## Grundlegendes

|  |  |
| --- | --- |
| Email-Adresse | schriftwart@kv-kiel.de |
| Vorgesetzte Stelle | 2. Vorsitzende |
| Nachgeordnete Stellen| keine |
| wird vertreten von | [Pressewartin]({{< relref "04-vorstand/ressorts/Pressewartin.md" >}}) |
| vertritt |  [Pressewartin]({{< relref "04-vorstand/ressorts/Pressewartin.md" >}}) |

## Geschäftsführungsaufgaben

1. Anfertigung von Protokollen für Vorstandssitzungen
1. Anfertigung von Protokollen für Mitgliederversammlungen
1. Gratulationskarten für Mitglieder über 60 Jahren
1. Durchführen von Korrespondenz mit Dritten
1. Regelmäßiges Kontrollieren des Posteingangs und die Verteilung der Post an die entsprechenden Stellen
1. Sicherstellen von Vorstandsterminen und rechtzeitige Absprache mit den Pächtern
1. Ausgabe von DKV-Ausweisen und Jahresmarken

## Vollmachten

|  |  |
| --- | --- |
| Allgemeine Vollmachten    |  |
| Kontovollmachten          |  |
| Sonstige Vollmachten      |  |
| Kompetenzen               | Anschaffen von Material für die Arbeit des Schriftwarts bis 100 Euro im Einzelfall |
