---
title   : "Bootshallenwartin"
subtitle: "Stellenbeschreibung"
weight	: 90
ressorts: [ Bootshallenwartin ]
date                    : 2019-12-16T19:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

## Grundlegendes

|  |  |
| --- | --- |
| Email-Adresse | bootshallenwart@kv-kiel.de |
| Vorgesetzte Stelle | 1. Vorsitzender |
| Nachgeordnete Stellen| keine |
| wird vertreten von | [Bauausschuss]({{< relref "04-vorstand/ressorts/Bauausschuss.md" >}}) |
| vertritt |  [Bauausschuss]({{< relref "04-vorstand/ressorts/Bauausschuss.md" >}}) |

## Geschäftsführungsaufgaben

1. Sicherstellen eines angemessenen Zustands der Bootshalle
1. Aufstellen eines Putzplans, Überwachung der Einhaltung des Putzplans
1. Sicherstellen, dass Toilettenpapier und Papierhandtücher jederzeit in ausreichendem Maße zur Verfügung stehen
1. Verwaltung der Bootsliegeplätze und Vereinsspinde
1. Ermahnen von Vereinsmitgliedern, die wiederholt die Ordnung in der Bootshalle stören

## Vollmachten

|  |  |
| --- | --- |
| Kompetenzen               | - Anschaffung von Material zur Instandhaltung und Verwaltung der Bootshalle bis 100 Euro im Einzelfall |
|                           | - Anschaffung von Putzmaterial zum Säubern der Halle bis 100 Euro im Einzelfall |
