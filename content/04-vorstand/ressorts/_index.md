---
title: Resorts
weight: 10
date                    : 2016-04-09T16:50:16+02:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---


Es gibt viel zu tun in so einem Verein. Das kann nicht eine alleine organisieren und so
haben wir wie in jedem Verein, eine Reihe von Ressorts geschaffen,
die  von den jeweils verantwortlichen Personen eigenständig organisiert werden.

{{% children depth="2" style="li" showhidden="true" %}}
