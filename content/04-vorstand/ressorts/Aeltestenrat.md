---
title   : "Ältestenrat"
subtitle: "Stellenbeschreibung"
weight	: 130
ressorts: [ Ältestenrat ]
date                    : 2019-12-16T19:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

## Grundlegendes

|  |  |
| --- | --- |
| Email-Adresse | aeltestenrat@kv-kiel.de |
| Vorgesetzte Stelle | Mitgliederversammlung |
| Nachgeordnete Stellen|  |
| Allgemeine Weisungsrechte | Gegenüber allen Mitgliedern auf Einhaltung der satzungsrechtlichen Vorschriften |
| wird vertreten von | (*niemand*) |
| vertritt | (*niemand*) |

## Geschäftsführungsaufgaben

1. Beratung des Vorstands
2. Unterstützung des Vorstands bei schwierigen Fragen bzgl. Mitgliedern und Satzung
3. Entscheidung über Sanktionen gegenüber Mitgliedern bis hin zum Ausschluss aus dem Verein
4. Kontrolle der Arbeit des Vorstand und ggf. Eingreifen, falls der Vorstand gegen die Weisungen der Mitgliederversammlung oder wider die satzungsgemäßen Bestimmungen des Vereins arbeitet
