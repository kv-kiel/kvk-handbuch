---
title   : "Bauausschuss"
subtitle: "Stellenbeschreibung"
weight	: 110
ressorts: [ Bauausschuss ]
date                    : 2019-12-16T19:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

## Grundlegendes

|  |  |
| --- | --- |
| Email-Adresse | bauausschuss@kv-kiel.de |
| Vorgesetzte Stelle | 1. Vorsitzender |
| Nachgeordnete Stellen| - |
| Allgemeine Weisungsrechte | Gegenüber allen Mitgliedern auf Einhaltung der satzungsrechtlichen Vorschriften |
| wird vertreten von | [Bauausschuss]({{< relref "04-vorstand/ressorts/Bauausschuss.md" >}}) |
| vertritt |  [Bauausschuss]({{< relref "04-vorstand/ressorts/Bauausschuss.md" >}}), [Bootshallenwartin]({{< relref "04-vorstand/ressorts/Bootshallenwartin.md" >}}) |

## Geschäftsführungsaufgaben

1. Erhalten des Gebäudes und des Grundstücks
1. Renovierung bedürftiger Gebäudeteile
1. Neubau von projektbezogenen An- Um- oder Einbauten
1. Beantragen von Geldern für Baumaßnahmen
1. Verwaltung der geleisteten Arbeitsdienststunden
1. Verwaltung des Bauausschuss-Etats
1. Vierteljährliche Abrechnung des Etat mit dem Kassenwart
1. Veranstalten von mind. 3 Arbeitsdiensten im Jahr
1. Bereitstellen von Material für Arbeitsdienste
1. Versorgen von Arbeitswilligen mit Arbeit
1. Überwachen der geleisteten Arbeitsstunden und Meldung an Mitgliederwart

## Vollmachten

|  |  |
| --- | --- |
| Allgemeine Vollmachten    | Etat des Bauausschuss |
| Kontovollmachten          |  |
| Sonstige Vollmachten      |  |
| Kompetenzen               | - Kauf von Material für Arbeitsdienste bis 500 Euro im Einzelfall |
|                           | - Einholen von Angeboten für größere Baumaßnahmen |
