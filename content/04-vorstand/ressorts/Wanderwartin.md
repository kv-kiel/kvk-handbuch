---
title   : "Wanderwartin"
subtitle: "Stellenbeschreibung"
weight  : 40
ressorts: [ Wanderwartin ]
date                    : 2020-01-24T16:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

## Grundlegendes

|                       |                                                                     |
| --------------------- | ------------------------------------------------------------------- |
| Email-Adresse         | wanderwartin@kv-kiel.de                                             |
| Vorgesetzte Stelle    | 1. Vorsitzende                                                      |
| Nachgeordnete Stellen | Referentin für Kieler Woche Turnier                                 |
| wird vertreten von    | [Kanupolowartin]({{< relref "04-vorstand/ressorts/kanupolowartin.md" >}}) |
| vertritt              | [Kanupolowartin]({{< relref "04-vorstand/ressorts/kanupolowartin.md" >}}) |
| Schlüssel             | ?                                                                   |
| Stempel               | Eckiger Stempel Text "Kanu-Vereinigung Kiel e.V."                   |

## Geschäftsführungsaufgaben

1. Sicherstellung eines geregelten Sportbetriebs im Bereich Wanderpaddeln
1. Bereitstellung geeigneten Materials zum Wanderpaddeln, Instandhalten des vereinseigenen Materials im Bereich Wanderpaddeln
2. Betreuung des Vereinstrailers (zusammen mit [Kanupolowartin](/kvk-handbuch/04-vorstand/ressorts/kanupolowartin/))
3. Organisation von Wanderpaddeltouren
4. Sicherstellung einer Sicherheitsgrundausbildung für alle Mitglieder
5. Sicherstellung, dass Vereinsmitglieder Vereinsmaterial nur im definierten Rahmen verwenden
6. Sicherstellen einer Grundausbildung im Bereich Umweltschutz für alle Mitglieder

## Vollmachten

|                        |                                                                                                               |
| ---------------------- | ------------------------------------------------------------------------------------------------------------- |
| Allgemeine Vollmachten |                                                                                                               |
| Kontovollmachten       |                                                                                                               |
| Sonstige Vollmachten   |                                                                                                               |
| Kompetenzen            | Kauf von Material zum Erhalten des vereinseigenen Materials im Bereich Wandersport bis 500 Euro im Einzelfall |
|                        | Einholen von Angeboten für die Anschaffung neuer Wanderboote                                                  |
