---
title   : "Kanupolowartin"
subtitle: "Stellenbeschreibung"
weight	: 70
ressorts: [ Kanupolowartin ]
date                    : 2020-01-24T16:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

## Grundlegendes

|                       |                                                                                       |
| --------------------- | ------------------------------------------------------------------------------------- |
| Email-Adresse         | polowart@kv-kiel.de                                                                  |
| Vorgesetzte Stelle    | 1. Vorsitzende                                                                        |
| Nachgeordnete Stellen | Referentin für Kieler Woche Turnier                                                   |
| wird vertreten von    | [Jugendwartin]({{< relref "04-vorstand/ressorts/Jugendwartin.md" >}})                 |
| vertritt              | [Wanderwartin]({{< relref "04-vorstand/ressorts/Wanderwartin.md" >}})                 |
| Schlüssel             | ?                                                                                     |
| Stempel               | Runder Stempel. "Kanu-Vereinigung Kiel e.V." im Kreis, in der Mitte die Vereinsflagge |
|                       | Eckiger Stempel Text "Kanu-Vereinigung Kiel e.V." - mit Adresse                       |
|                       | Eckiger Stempel Text "Kanu-Vereinigung Kiel e.V." - ohne Adresse                      |

## Geschäftsführungsaufgaben

1. Sicherstellung eines geregelten Sportbetriebs
2. Bereitstellung geeigneten Materials zum Kanupolospielen
3. Organisation von Kanupolo-Veranstaltungen
4. Regelung der Trainingszeiten
5. Sicherstellung der Schiedsrichterausbildung
6. Sicherstellung, dass Vereinsmitglieder Vereinsmaterial nur im definierten Rahmen verwenden
7. Betreuung des Vereinstrailers (zusammen mit [Wanderwartin](/kvk-handbuch/04-vorstand/ressorts/wanderwartin/))
8. Instandhaltung des gesamten Kanupolomaterials (außer reines Jugendmaterials)
9.  Sicherstellen der Ordnung im Kanupoloschuppen

## Vollmachten

|                        |                                                                                           |
| ---------------------- | ----------------------------------------------------------------------------------------- |
| Allgemeine Vollmachten |                                                                                           |
| Kontovollmachten       |                                                                                           |
| Sonstige Vollmachten   |                                                                                           |
| Kompetenzen            | Kauf von Material für die Instandhaltung des Kanupolomaterials bis 500 Euro im Einzelfall |
|                        | Bezahlung der Startgelder bei Turnieren nach eigenem Ermessen                             |
|                        | Einholen von Angeboten für Kanupolo-Material                                              |
