---
title   : "Kassenwartin"
subtitle: "Stellenbeschreibung"
weight	: 30
ressorts: [ Kassenwartin ]
date                    : 2019-12-16T19:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

<!-- TOC -->autoauto1. [1. Grundlegendes](#1-grundlegendes)auto2. [2. Geschäftsführungsaufgaben](#2-geschäftsführungsaufgaben)auto3. [3. Vollmachten](#3-vollmachten)auto4. [4. Detailbeschreibung einzelner Aufgaben](#4-detailbeschreibung-einzelner-aufgaben)auto    1. [4.1. Übungsleiterzuschüsse beantragen](#41-übungsleiterzuschüsse-beantragen)autoauto<!-- /TOC -->

## 1. Grundlegendes

|                       |                                                                           |
| --------------------- | ------------------------------------------------------------------------- |
| Email-Adresse         | kassenwart@kv-kiel.de                                                     |
| Vorgesetzte Stelle    | Vorstand nach § 26 BGB, Mitgliederversammlung                             |
| Nachgeordnete Stellen | Mitgliederwart                                                            |
| wird vertreten von    | [1. Vorsitzende]({{< relref "04-vorstand/ressorts/1.Vorsitzende.md" >}}), |
|                       | [2. Vorsitzende]({{< relref "04-vorstand/ressorts/2.Vorsitzende.md" >}})  |
| vertritt              | [1. Vorsitzende]({{< relref "04-vorstand/ressorts/1.Vorsitzende.md" >}}), |
|                       | [2. Vorsitzende]({{< relref "04-vorstand/ressorts/1.Vorsitzende.md" >}})  |
| Schlüssel             | Vorstandsspind, Kassenwart-Spind                                          |
| Stempel               | -                                                                         |

## 2. Geschäftsführungsaufgaben

1. Durchführung der Vereinsbuchführung incl. Jahresabschlüsse und Statistiken
2. Zahlung von Übungsleiterentschädigungen
3. Sicherstellung der Beachtung steuerlicher Vorschriften in allen Bereichen des Vereins
4. Aufnahme und Sicherstellung der mobilen und immobilen Vermögenswerte des Vereins
5. Sicherstellung eines umfangreichen Wissens im Bereich Kassenführung im Verein
6. Überwachen des aktuellen Kassenstandes und ggf. rechtzeitiges Warnen vor unvorhergesehenen Ausgaben
7. Erstellen eines Haushaltsvoranschlags für die nachfolgende Saison

## 3. Vollmachten

|                        |                                                                                                           |
| ---------------------- | --------------------------------------------------------------------------------------------------------- |
| Allgemeine Vollmachten | Vertretung des Vereins nach §26 BGB zusammen mit einem weiteren Mitglied des Geschäftsführenden Vorstands |
| Kontovollmachten       | Geschäftskonto bei der Sparkasse, alle Konten                                                             |
| Sonstige Vollmachten   |                                                                                                           |
| Kompetenzen            | - Einziehen der Mitgliedsbeiträge                                                                         |
|                        | - Mahnung von Mitgliedern, die ihren finanziellen Verpflichtungen nicht nachkommen                        |
|                        | - Ausstellen von Spendenbescheinigungen                                                                   |
|                        | - Zahlungen des Vereins an Gläubiger                                                                      |
|                        | - Einziehen der Pacht aus Verpachtungsverhältnissen                                                       |

## 4. Detailbeschreibung einzelner Aufgaben

### 4.1. Kassenprüfung durchführen lassen

Einmal jährlich - vor der Mitgliederversammlung - muss eine Kassenprüfung durchgeführt werden. Hier ist eine ganz 
[gute Beschreibung](https://www.vereinswelt.de/kassenbericht-pruefbericht),
 worauf dabei zu achten ist.

 Die Kassenwartin lädt zu gegebener Zeit die beiden gewählten Kassenprüferinnen ein und legt ihnen die zu prüfenden Unterlagen vor.
  Zur Kassenprüfung gehört auch die Durchsicht des Jahresabschlusses des Mitgliederverwaltungsprogrammmes.

### 4.2. Übungsleiterzuschüsse beantragen

- **Antrag ist abzugeben bis zum 31. Mai jeden Jahres**

Die Stadt Kiel stellt dem Sportverband Kiel für die Bezuschussung von nebenamtlichen Übungsleitern jährlich Gelder zur Verfügung.
Diese werden nach einem bestimmten Schlüssel auf die Vereine verteilt.

> Für jeden gemeldeten Übungsleiter ist als Nachweis eine Ablichtung des gültigen Qualifikationsnachweises dem Antragsformular beizufügen.
> Weiterhin muss dem Sportverband die Satzung und die gültige Freistellungsbescheinigung vorliegen (sollten die/der Satzung/Freistellungsbescheid in der dem Antrag anliegenden Liste aufgeführt sein, müssen beide nicht mehr vorgelegt werden, außer der Freistellungsbescheid ist nicht mehr gültig). Ohne diese Nachweise werden Übungsleiter nicht anerkannt.

Die Zuschüsse werden unter folgenden Randbedingungen gewährt

1. der Verein muss **mindestens 10 jugendliche Mitglieder** gemeldet haben
2. für Übungsleiter werden pro _volle_ 40 Mitglieder 1 lizensierter Übungsleiter berücksichtigt
3. Mitgliederzuschüsse werden nur an Vereine gezahlt, die mindesten 1 lizensierten Übungsleiter gemeldet haben
