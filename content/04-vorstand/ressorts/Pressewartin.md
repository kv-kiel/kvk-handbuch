---
title   : "Pressewartin"
subtitle: "Stellenbeschreibung"
weight	: 100
ressorts: [ Pressewartin ]
date                    : 2020-01-24T15:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

## Grundlegendes

|  |  |
| --- | --- |
| Email-Adresse | pressewart@kv-kiel.de |
| Vorgesetzte Stelle | 2. Vorsitzende |
| Nachgeordnete Stellen| Redaktion Vereinszeitung, Redaktion Homepage |
| wird vertreten von | [Schriftwartin]({{< relref "04-vorstand/ressorts/Schriftwartin.md" >}}) |
| vertritt |  [Schriftwartin]({{< relref "04-vorstand/ressorts/Schriftwartin.md" >}}) |

## Geschäftsführungsaufgaben

1. Schreiben von Artikeln über den Verein, seine Aktivitäten und Vorstandsarbeiten
1. Sicherstellen einer angemessenen Pressearbeit im Verein
1. Begleiten von Veranstaltungen des Vereins im Rahmen der Öffentlichkeit
1. Pflege des Veranstaltungskalenders
1. Pflege der Flyer und Sicherstellen einer angemessenen Aktualität
1. Sammlung und Archivierung aller Veröffentlichungen über den Verein
1. Pflege von persönlichen Beziehungen zu Pressevertretern von Zeitung und anderen Medien
1. Sicherstellen einer angemessenen Optik und eines Werbewirksamen Inhalts der Schaukästen des Vereins
1. Archivierung von Presseartikel etc.
1. Ausgabe der Vereinswimpel

## Vollmachten

|  |  |
| --- | --- |
| Allgemeine Vollmachten    |  |
| Kontovollmachten          |  |
| Sonstige Vollmachten      |  |
| Kompetenzen               |  |
|                           |  |
