---
title   : "Jugendwartin"
subtitle: "Stellenbeschreibung"
weight	: 50
ressorts: [ Jugendwartin ]
date                    : 2019-12-16T19:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

## Grundlegendes

|  |  |
| --- | --- |
| Email-Adresse | jugendwartin@kv-kiel.de |
| Vorgesetzte Stelle | 2. Vorsitzende |
| Nachgeordnete Stellen| keine |
| wird vertreten von | [Stellv. Jugendwartin]({{< relref "04-vorstand/ressorts/Jugendwartin.md" >}}) |
| vertritt |  [Kanupolowartin]({{< relref "04-vorstand/ressorts/kanupolowartin.md" >}}) |

## Geschäftsführungsaufgaben

1. Sicherstellung einer angemessenen Jugendarbeit
1. Ansprechpartner für die Erziehungsberechtigten jugendlicher Vereinsmitglieder
1. Organisation und Durchführung von Jugendfahrten
1. Sicherstellung eines geregelten Trainingsbetriebes für Jugendliche
1. Sicherstellung der Integration Jugendlicher in den Verein
1. Durchführen der Jugendversammlung
1. Anschaffung und Pflege von Material für das Training Jugendlicher
1. Vertretung jugendlicher Anliegen im Vorstand
1. Sicherstellung einer angemessenen Ausbildung aller Jugendbetreuer
1. Sicherstellung einer angemessenen Ausbildung Jugendlicher im Bereich Sicherheit auf dem Wasser und Umweltschutz

## Vollmachten

|  |  |
| --- | --- |
| Allgemeine Vollmachten    |  |
| Kontovollmachten          |  |
| Sonstige Vollmachten      |  |
| Kompetenzen               | - Anschaffung von Material für die Jugendarbeit im Rahmen des Jugendetats bis 500 Euro im Einzelfall |
|                           | - Einholen von Angeboten für die Anschaffung von Material für die Jugendarbeit |
