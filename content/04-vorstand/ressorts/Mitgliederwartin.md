---
title   : "Mitgliederwartin"
subtitle: "Stellenbeschreibung"
weight	: 60
ressorts: [Mitgliederwartin]
date                    : 2021-01-11T11:23:26+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

- [1. Grundlegendes](#1-grundlegendes)
- [2. Geschäftsführungsaufgaben](#2-geschäftsführungsaufgaben)
- [3. Vollmachten](#3-vollmachten)
- [4. Termine Bankeinzüge](#4-termine-bankeinzüge)
- [5. Detailbeschreibung einzelner Aufgaben](#5-detailbeschreibung-einzelner-aufgaben)
  - [5.1. Aktionen bei Neueintritt](#51-aktionen-bei-neueintritt)
    - [5.1.1. Eingang des Aufnahmeformulars im Verein](#511-eingang-des-aufnahmeformulars-im-verein)
    - [5.1.2. Formale Aufnahme bei Vorstandssitzung](#512-formale-aufnahme-bei-vorstandssitzung)
    - [5.1.3. Zuschicken folgender Dokumente an neues Mitglied](#513-zuschicken-folgender-dokumente-an-neues-mitglied)
    - [5.1.4. Begrüßung der neuen Mitglieder](#514-begrüßung-der-neuen-mitglieder)
  - [5.2. Aktionen bei Austritt](#52-aktionen-bei-austritt)
    - [5.2.1. Eingangsbestätigung](#521-eingangsbestätigung)
    - [5.2.2. Verabschiedung](#522-verabschiedung)
    - [5.2.3. Rücküberweisung Schlüsselpfand](#523-rücküberweisung-schlüsselpfand)
  - [5.3. Einziehen von Beiträgen und Gebühren](#53-einziehen-von-beiträgen-und-gebühren)
  - [5.4. Verfolgung von Zahlungsverzügen](#54-verfolgung-von-zahlungsverzügen)
  - [5.5. Stärkemeldungen an LKV und LSV](#55-stärkemeldungen-an-lkv-und-lsv)
- [6. Mitgliederverwaltung](#6-mitgliederverwaltung)
## 1. Grundlegendes

|                       |                                                                                          |
| --------------------- | ---------------------------------------------------------------------------------------- |
| Email-Adresse         | mitgliederwart@kv-kiel.de                                                                |
| Vorgesetzte Stelle    | Kassenwartin                                                                             |
| Nachgeordnete Stellen | keine                                                                                    |
| wird vertreten von    | [Kassenwartin]({{< relref "04-vorstand/ressorts/Kassenwartin.md" >}})                    |
| vertritt              | [Kassenwartin]({{< relref "04-vorstand/ressorts/Kassenwartin.md" >}}) (nicht gesetzlich) |
| Schlüssel             | Kassenwart-Spind                                                                         |

## 2. Geschäftsführungsaufgaben

1. Einziehen und Verwalten der Mitgliedsbeiträge
2. Pflege der Mitgliederdatenbank
3. Ausstellen geeigneter Statistiken für interne und externe Zwecke
4. Schreiben von Eintritts- und Austrittsbriefen an neue bzw. austrittswillige Mitglieder
5. Verwaltung der Vereinsschlüssel und des zugehörigen Schlüsseletats
6. Einziehen der Arbeitdsdienstersatzzahlungen

## 3. Vollmachten

|                        |     |
| ---------------------- | --- |
| Allgemeine Vollmachten |     |
| Kontovollmachten       |     |
| Sonstige Vollmachten   |     |
| Kompetenzen            |     |

## 4. Termine Bankeinzüge

Um eine gleichmäßige Verteilung der Belastung der Mitgliederkonten zu gewährleisten und einen vollständigen Jahresabschluss sicherzustellen, wurden folgende Termine für die SEPA-Einzugsläufe festgelegt:

| Termin    | Einzug                         | Bezugszeitraum                        |
| --------- | ------------------------------ | ------------------------------------- |
| März      | Mitgliedsbeiträge              | erstes Halbjahr des laufenden Jahres  |
| Juni      | Ersatzzahlungen Arbeitsdienste | erstes Halbjahr des laufenden Jahres  |
| September | Mitgliedsbeiträge              | zweites Halbjahr des laufenden Jahres |
| Dezember  | Ersatzzahlungen Arbeitsdienste | zweites Halbjahr des laufenden Jahres |

Dadurch, dass ein Austritt nur zum Quartalsende möglich ist, ist sichergestellt, dass nach dem Austrittstermin keine Beitragsforderungen mehr eingezogen werden. Bei Mitgliedern, die zum 31. März bzw. 30. September austreten, muss im Mitgliederverwaltungsprogramm explizit dafür Sorge getragen werden, dass nicht der volle Halbjahresbeitrag eingezogen wird. 

Da die Ersatzzahlungen für nicht geleistete Arbeitsdienste aber erst am Ende eines Halbjahres berechnet und eingezogen werden, kommt es bei einem Austritt zum 31. März bzw. 30. September unter Umständen dazu, dass auch nach dem Austrittstermin noch eine Abbuchung vom Konto des bereits ausgetretenen Mitglieds vorgenommen wird. Auf diesen Umstand ist das Mitglied im Aufnahmeformular hingewiesen worden.

## 5. Detailbeschreibung einzelner Aufgaben

### 5.1. Aktionen bei Neueintritt

Bei Neueintritte wird grundsätzlich das Aufnahmeformular ausgefüllt. Der Prozess vom Ausfüllen des Formulars bis zum wirksamen Vereinsbeitritt soll wie im Folgenden beschrieben gehandhabt werden.

#### 5.1.1. Eingang des Aufnahmeformulars im Verein

Aufnahmeformulare sollen in des Spind des Vorsitzenden geworfen werden.

Jeder, der von einem Eintrittswilligen einen Aufnahmeantrag entgegennimmt oder irgendwo im Verein "findet", soll das Papier in den dafür vorgesehenen Spind werfen.

Es wird keine separate Eingangsbestätigung verschickt.

#### 5.1.2. Formale Aufnahme bei Vorstandssitzung

Auf der Vorstandssitzung werden die eingegangenen Aufnahmeanträge verlesen und zur Abstimmung gebracht. Die Anträge der Neumitglieder werden abgestempelt und dem Mitgliederwart zum Abheften übergeben. Ist der Mitgliederwart nicht anwesend, werden die Anträge im Spind der Kassenwartin deponiert, wo sie vom Mitgliederwart abgeholt werden.

#### 5.1.3. Zuschicken folgender Dokumente an neues Mitglied

Wenn vom neuen Mitglied eine EMail-Adresse vorliegt, werden folgende Dokumente verschickt:

1. Anschreiben mit Eintrittstermin und Einladung zur nächsten Vorstandssitzung (mit Termin)
2. Willkommen in der KVK
3. Satzung
4. Gebührenordnung

Bis auf das Anschreiben können die anderen Dokumente auch als Link verschickt werden.

#### 5.1.4. Begrüßung der neuen Mitglieder

In jeder Ausgabe der Vereinszeitung werden die seit der letzten Ausgabe neu aufgenommenen Mitglieder namentlich erwähnt.

Auf den Vorstandsitzungen werden die neu aufgenommenen Mitglieder begrüßt. Vorstand und Neumitglieder stellen sich gegenseitig vor.

### 5.2. Aktionen bei Austritt

Austritte erfolgen entweder durch EMail oder durch Einsendung/Abgabe eines Austrittsgesuches.

Jeder, der ein Austrittsgesuch entgegennimmt oder irgendwo im Verein "findet", informiert unverzüglich die Mitgliederwartin oder ein Mitglied des geschäftsführenden Vorstandes. Erfolgt diese Benachrichtigung per EMail, sollte diese EMail auch an das austretende Mitglied gehen.

Es ist wichtig, bei der Benachrichtigung per EMail die Funktionsadressen zu benutzen und nicht die persönlichen EMail-Adresse der aktuellen Amtsinhaber. Über die Funktionsadressen ist bereits eine Vertreterregelung sichergestellt.

#### 5.2.1. Eingangsbestätigung

Jedes Austrittsgesuch wird möglichst umgehend mit einer Eingangsbestätigung per EMail beantwortet.

Erfolgt der Austritt "in Papierform", so ist das Schreiben in den Spind des Vorsitzenden zu werfen. Natürlich ist trotzdem der geschäftsführende Vorstand und/oder der Mitgliederwart wie oben beschrieben zu benachrichtigen.

Ist dem Schreiben keine EMail zu entnehmen, versucht der Mitgliederwart eine EMail-Adresse zu ermitteln und diese für die Eingangsbestätigung zu nutzen.

#### 5.2.2. Verabschiedung

Die gesammelten Austrittsgesuche werden auf jeder Vorstandssitzung verlesen, abgestempelt und dem Mitgliederwart zur Archivierung übergeben. Ist der Mitgliederwart auf der Vorstandssitzung nicht zugegen, werden die abgestempelten Anträge im Spind der Kassenwartin deponiert (Mitgliederwart hat Schlüssel hierfür).

In jeder Ausgabe der Vereinszeitung werden die  Mitglieder namentlich erwähnt, die seit der letzten Ausgabe einen Austrittsantrag gestellt haben.

Der Austretende erhält ein Anschreiben, das folgende Informationen enthält:

1. Angabe des Termins, wann Austritt wirksam wird.
2. Hinweis, dass auch noch nach diesem Termin Abbuchungen erfolgen (Ersatzzahlungen für nciht geleistete Arbeitsdienste),
3. Angabe des Termins der abschließenden Abbuchung
4. Hinweis auf Schlüsselabgabe - Schlüsselpfand verfällt, wenn Schlüsselrückgabe nicht vor der abschließenden Abbuchung erfolgt

#### 5.2.3. Rücküberweisung Schlüsselpfand

Bei Austritt wird das vom Mitglied eingezogene Schlüsselpfand mit der abschließenden Abbuchung zurückerstattet.
Voraussetzung ist, dass der ausgegebene Hallenschlüssel vor dem Termin der abschließenden Abbuchung zurückgegeben wird.
Bei einer späteren Rückgabe verfällt das Schlüsselpfand.

### 5.3. Einziehen von Beiträgen und Gebühren

Die Mitgliedsbeiträge werden halbjährlich in der Mitte des Halbjahres eingezogen (März und September). Versetzt dazu werden die Ersatzzahlungen für nicht geleistete Pflichtstunden beim Arbeitsdienst eingezogen.

Die Erfassung und Verwaltung der Arbeitsdienste erfolgt durch den
[Bauausschuss]({{< relref "04-vorstand/ressorts/Bauausschuss.md" >}}). Dafür wird das Programm 
[ADHelper](http://baltic-mh.github.io/ADHelper/) eingesetzt.

Zu den Terminen, an denen die Bankeinzüge für die Arbeitsdienstersatzzahlungen durchzuführen sind,
muss _ADHelper_ einmal gestartet werden, um die Daten zu aktualisieren.
Aus dem Datenverzeichnis für die aktuelle Abrechnungsperiode muss die Datei _ZuZahlendeStunden.csv_
entnommen werden.
In ihr ist für alle Mitglieder, die ihrer Arbeitsdienstpflicht nicht in ausreichendem Maß nachgekommen sind, die Anzahl der Stunden angegeben, für die Ersatzzahlungen geleistet werden müssen.
Daraus ist mit dem Vereinsverwaltungsprogramm ein Einzugslauf zu erzeugen und durchzuführen.

### 5.4. Verfolgung von Zahlungsverzügen

Siehe [Zahlungsverzug]({{< relref "zahlungsverzug.md" >}}).

### 5.5. Stärkemeldungen an LKV und LSV

{{% notice warning %}}
Muss noch mit (mehr) Leben erfüllt werden
{{% /notice %}}

Anfang des Jahres müssen die aktuellen Mitgliederzahlen an den LSV _und_ an den LKV gemeldet werden.
Die Meldung an den LSV erfolgt online ( https://s-h.lsb-be.de/ ).
Für die Meldung an den LKV erhält der Vorsitzende mit der Einladung zum Verbandstag einen Vordruck, der ausgefüllt an die Kassenwartin des Verbands geschickt werden muss. Dabei soll auch eine Ausfertigung an den Verbandsvorsitzenden geschickt werden.

Die Meldung der Mitgliederzahlen muss für festgelegte Altersgruppen erfolgen:

| Altersgruppe       | männlich | weiblich | insgesamt |
| ------------------ | :------: | :------: | :-------: |
| 0 - 6 Jahre        |    1     |    1     |     2     |
| 7 - 14 Jahre       |    10    |    9     |    19     |
| 15 - 18 Jahre      |    7     |    4     |    11     |
| 19 - 26 Jahre      |    6     |    10    |    16     |
| 27 - 40 Jahre      |    43    |    25    |    68     |
| 41 - 60 Jahre      |    44    |    37    |    81     |
| 61 Jahre und älter |    12    |    6     |     4     |
| **Gesamt**         | **123**  |  **92**  |  **215**  |

Eine passende Statistik kann über das Programm für die Mitgliederverwaltung erzeugt werden.

## 6. Mitgliederverwaltung

Die Verwaltung der Mitgliederdaten und der zugehörigen Banktransaktionen wird über das Vereinsverwaltungsprogramm **SPG Verein** vorgenommen. Für die Bedienung dieses Programms gibt es eine eigene [Web-Seite](https://kv-kiel.gitlab.io/kvk-doku-center/kvk-handbuch-spg-verein/index.html).