---
title   : "Einsteigertraining"
weight	: 20
tags	: []
ressorts: [ Wanderwartin ]
date                    : 2020-02-07T17:14:35+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

![Einsteigergruppe](Einsteigergruppe.JPG)

## 1. Inhalt
<!-- TOC -->

- [1. Inhalt](#1-inhalt)
- [2. Grundlegendes](#2-grundlegendes)
- [3. Anmeldung](#3-anmeldung)
- [4. Ablauf](#4-ablauf)
  - [4.1. Begrüßung](#41-begr%c3%bc%c3%9fung)
  - [4.2. Zuweisung der Boote](#42-zuweisung-der-boote)
  - [4.3. Fertigmachen der Boote](#43-fertigmachen-der-boote)
  - [4.4. Auf dem Steg](#44-auf-dem-steg)
  - [4.5. Auf dem Wasser](#45-auf-dem-wasser)
  - [4.6. Nach dem Paddeln](#46-nach-dem-paddeln)

<!-- /TOC -->
## 2. Grundlegendes

Jedes Jahr ab Mitte Mai bieten wir ein Einsteigertraining an (für den genauen Termin im aktuellen Jahr bitte einen Blick auf die
[Homepage](https://kv-kiel.de/wanderpaddeln/training)
werfen. Auch, was man dafür an eigener Ausrüstung mitbringen sollte ist dort
[aufgezählt](https://kv-kiel.de/wanderpaddeln/mitmachen) ).

Das Einsteigertraining ist für absolute  Neulinge und noch nicht so sicher paddelnde Interessenten gedacht. Eine bestehende Vereinsmitgliedschaft bei uns ist keine Pflicht - aber auch kein Hinderungsgrund. Auch Jugendliche sind willkommen, wir bieten jedoch keine separate Gruppe an.

## 3. Anmeldung

Da wir bei diesen Terminen den Teilnehmern sowohl umfangreiche Ausrüstung anbieten sowie eine kompetente Betreuung garantieren müssen, ist es unerlässlich, sich vor jedem Termin bei der [Wanderwartin](mailto:wanderwart@kv-kiel.de) anzumelden.

Da in der Regel eine Anzahl von Paddlern mit nur sehr geringen Kenntnissen und Vorerfahrungen an diesem Training teilnimmt, lässt es sich nur durchführen, wenn eine hinreichende Anzahl an unterstützenden Paddlern zur Verfügung steht. Es wird daher eine Liste geführt, in die sich Paddler eintragen können, die als Unterstützung helfen wollen. Der für das Training Verantwortliche entscheidet, ob die Anzahl von angemeldeten Unterstützern eine Durchführung des Termins unter den aktuellen Bedingungen zulässt.

## 4. Ablauf

Beim Einsteigertraining kommen häufig Gäste zu uns, die noch nie gepaddelt sind - sich zumindest aber mit den Abläufen bei uns im Verein nicht auskennen. Hier ein kurzer Abriss, worauf die Helfer achten bzw. worauf sie die Teilnehmer hinweisen müssen.

### 4.1. Begrüßung

1. Einweisen in die Örtlichkeiten (Toiletten, Umkleide)
2. Eintragen in die Teilnehmerliste
3. Unterschreiben des Haftungsausschlusses
4. Umziehen
5. Wertsachen entgegennehmen und in Spind deponieren
6. Ausrüstung zuweisen
    - Schwimmweste
    - Paddel

### 4.2. Zuweisung der Boote

Das im folgenden beschriebene Vorgehen hat sich bewährt:

1. Bei Eintreffen eines Teilnehmers wird ein Boot bestimmt, das auf den Rasen gelegt wird. Dieses Boot ist nicht zwangsläufig für diesen Teilnehmer bestimmt.
2. Die auf den Rasen gebrachten Boote werden in zwei Abschnitten abgelegt:
    - Boote, bei denen feststeht, wer in ihnen fährt, näher zur Hecke positioniert
    - Boote, für die noch ein passender Paddler bestimmt werden muss, näher zur Bootshalle.
3. Alle Teilnehmer, denen noch kein Boot zugewiesen worden ist, stellen sich in einer Gruppe zusammen.
4. Der Verantwortliche weist nun jedem Teilnehmer ein ihm passendes Boot zu.
5. Eintragen ins Fahrtenbuch

### 4.3. Fertigmachen der Boote

1. Fußstützen einstellen
2. Lukendecken schließen
3. Boote zum Wasser tragen

### 4.4. Auf dem Steg

Auf dem Steg versammeln sich alle Teilnehmer noch einmal.
Der Durchführende hält ein kurzes Briefing ab, in dem kurz die aktuellen Bedingungen abgefragt werden (Windstärke, -richtung, Luft- und Wassertemperatur, zu erwartende Besonderheiten, etc.).

Mindestens ein erfahrener Paddler geht zuerst aufs Wasser, um weniger sichere Paddler während des Einsetzvorganges von der Wasserseite her unterstützen zu können.

Bei Personen, die das erste Mal dabei sind, ist unbedingt darauf hinzuweisen, dass die Schlaufe zum Öffnen der Spritzdecke frei außen liegen muss. Nach dem Schlißen der Spritzdecke ist unbedingt das Öffnen der dann geschlossenen Decke einmal im Boot sitzend zu praktizieren.
Außerdem sind sie ausdrücklich darauf hinzuweisen, dass sie im Falle einer ungewollten Kenterung nach vorne zur Schlaufe greifen und damit die Spritzdecke öffnen sollen.

### 4.5. Auf dem Wasser

### 4.6. Nach dem Paddeln

1. Boote spülen
2. Ausrüstung zurückbringen
3. Austragen aus dem Fahrtenbuch
4. Auf Teilnehmerliste vermerken, wer Wiedereinstieg durchgeführt hat.
5. Bei drittmaligem Erscheinen eines Neulings Aufnahmeantrag und Satzung aushändigen
    - Hinweis auf Pflicht zum Arbeitsdienst
    - Hinweis auf Mindestdauer der Mitgliedschaft von einem Jahr
6. Beim fünften Erscheinen eines Neulings Abgabe des Aufnahmeantrags prüfen
7. Wertsachen zurückgeben
8. Halle abschließen
