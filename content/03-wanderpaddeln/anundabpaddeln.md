---
title 	: "An- und Abpaddeln"
tags	: [Veranstaltungen]
ressorts: [Wanderwartin]
weight  : 10
date  	: 2019-11-15T12:00:22+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

![Anpaddeln](../Anpaddeln.jpg)
Das Paddeljahr wird traditionell mit dem *Anpaddeln* etwa Mitte April eröffnet und mit dem *Abpaddeln* etwa Mitte Oktober beendet.

Beide Veranstaltungen sind dergestalt, dass einer der fünf "Kieler" Kanuvereine die Organisation übernimmt und die anderen vier einlädt.
Die Paddler aller Vereine kommen dann zu einem verabredeten Zeitpunkt zusammen und paddeln in mehr oder weniger großen Grüppchen so weit, wie es jedem gefällt.
Meistens wird auf der Schwentine gepaddelt, weil versucht wird, zu diesen Veranstaltungen möglichst viele Paddler aufs Wasser zu bekommen und nicht jeder sich traut, in der frühen bzw. vorgerückten Jahreszeit auf der offenen Förde zu paddeln.

Nachdem das Bewegungsbedürfnis gestillt ist, kommt man beim gastgebenden Verein zusammen, wo bereits Kaffee, Kuchen, Würstchen, Waffeln und was das kulinarische Herz sich sonst noch wünschen kann, auf die hungrigen Seelen warten.

Hier steht vor allem die Begegnung, das Kennenlernen und der Austausch im Vordergrund.
Aber auch die Gelegenheit, die Schwentine im Frühling im Aufbruch zu sehen, die noch schlammverschmierten Schildkröten nach ihrer Winterpause wieder zu begrüßen oder im Herbst die ruhige Stimmung mit den bunten Blättern der Bäume und den hektisch am Ufer von Strauch zu Strauch eilenden Eisvögeln zu genießen.
All das macht diese Veranstaltungen zu einem lohnenswerten Event, an dem man jedes Jahr gerne wieder teilnimmt.
