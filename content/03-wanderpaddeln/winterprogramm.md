---
title 	: "Winterprogramm"
tags	: [Veranstaltungen]
ressorts: [Wanderwartin]
weight  : 100
date                    : 2019-12-19T19:22:51+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

In der dunklen Jahreszeit sind die Termine, bei denen die Wanderpaddler draußen auf dem Wasser sind, seltener. Man ist mit dem aktivem Ausüben unseres Sportes quasi auf die Wochenenden beschränkt.

Um aber trotzdem das Vereinsleben am Laufen zu halten, findet jeden Mittwoch im Vereinsheim eine Veranstaltung statt, deren Themen so unterschiedlich sind, wie unsere Paddler selbst.

So gibt es Berichte, wenn jemand eine Fahrt oder eine Unternehmung gemacht hat, über die er seine Vereinskameraden unterrichten möchte, es gibt Fachvorträge von Mitgliedern, die in bestimmten Themen besonders versiert sind. Jedes Jahr gibt es einen Bilderabend, in dem die gesammelten Fotos der vergangenen Saison gezeigt werden und man sich gemeinsam an die durchlebten Abenteuer erinnert.

Daneben haben auch Spieleabende ihren festen Platz in unserem Programm gefunden, Doppelkopf-Events und Abende, an denen wir einfach Lieder singen, wozu man im "normalen" Leben ja viel zu selten kommt.

Es lohnt sich auf jeden Fall, einen wachen Blick auf unseren
[Veranstaltungskalender](https://kv-kiel.de/kalender)
zu halten.
