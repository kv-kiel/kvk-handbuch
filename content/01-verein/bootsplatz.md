---
title 	: "Bootsplatz"
date  	: 2020-08-18T16:56:32+02:00
tags	: [Bootshalle]
ressorts: [Bootshallenwartin, Kanupolowartin]
weight  : 80
---

![Bootsplatz](../Bootsplatz.jpg)

Du möchtest einen Platz für dein Boot in unserer Halle?

Da wir immer wieder Anfragen in dieser Richtung bekommen: Wir vermieten keine Bootsplätze unabhängig von einer Mitgliedschaft.
Wir sind grundsätzlich viel eher daran interessiert, Boote auf dem Wasser zu benutzen, als sie in der Halle liegen zu sehen. Wenn es nur darum geht, ein Boot unterzubringen, findet sich viel leichter anderswo ein Platz, weil dafür ja nicht unbedingt eine absolute Nähe zum Wasser gegeben sein muss.

Bootsplätze für Seekajaks werden nach Verfügbarkeit von der
[Bootshallenwartin](mailto:bootshallenwart@kv-kiel.de)
zugeteilt. 
Für die Unterbringung privater Poloboote ist die
[Kanupolowartin](mailto:polowart@kv-kiel.de)
zuständig.

Aktuell sind alle unsere Bootsplätze belegt und es existiert eine Warteliste.
Die gute Nachricht ist aber, dass eine relativ hohe Fluktuation bei den Bootsplätzen herrscht,
so dass die Aussicht, nach nicht allzu langer Wartezeit einen Platz zu ergattern, nicht gar so finster ist.

Bootsplätze werden wie oben angedeutet generell nur an Vereinsmitglieder vergeben.
Man muss also erst
[Mitglied werden,](../eintritt/index.html)
 dann einen [Antrag auf einen Bootsplatz](https://kv-kiel.de/verein/downloads) stellen (den man auch zusammen mit dem [Aufnahmeantrag](https://kv-kiel.de/verein/downloads)
abgegeben kann) und dann warten, bis man an der Reihe ist.
Es funktioniert also nicht, den Vereinseintritt so lange hinauszuschieben, bis man mit Sicherheit einen Bootsplatz bekommt.

Verschickt man den ausgefüllten Antrag auf Zuteilung eines Bootsplatzes (oder alle im Antrag benötigten Informationen) per EMail an die 
[Bootshallenwartin](mailto:bootshallenwart@kv-kiel.de),
erhält man eine Antwortmail, die auch Auskunft über die eigene Position auf der Warteliste gibt.

Ein Anspruch auf einen bestimmten Liegeplatz besteht aber auch nach der Zuteilung
nicht.

Eine zeitweilige Lagerung eines Bootes von ein bis zwei Wochen – zum Beispiel zum
Testen eines Bootes kurz vor dem Kauf oder in anderen besonderen Fällen - ist vor Einlagerung des Bootes mit der Bootshallenwartin abzustimmen.
Sollte diese nicht erreichbar sein, muss der Umstand mit dem geschäftsführenden Vorstand abgeklärt werden.

Pro Mitglied wird nur ein Bootsplatz vergeben. Die Bootsplätze sind ausschließlich zur Lagerung von Booten gedacht. Ist ein Bootsplatz für mehr als drei Monate nicht mit einem Boot belegt, kann er nach Rücksprache mit dem Nutzer durch den Vorstand neu vergeben werden.

Spinde werden ebenfalls von der
[Bootshallenwartin](mailto:bootshallenwart@kv-kiel.de)
zugeteilt. Die Beantragung erfolgt formlos über eine EMail.
Zwar gibt es aktuell noch keine Warteliste für Spinde, aber da auch hier das Angebot begrenzt ist, erfolgt die Zuweisung grundsätzlich analog wie die der Bootsplätze.