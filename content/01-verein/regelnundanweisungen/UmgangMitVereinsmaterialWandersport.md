---
title                   : "Regeln zum Umgang mit Vereinsmaterial im Kanuwandersport"
tags                    : []
weight                  : 10
ressorts                : [Wanderwartin]
date                    : 2012-08-06T16:30:35+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

Alle aktiven Vereinsmitglieder können das vom Verein bereitgestellte Material nach einer kurzen Einweisung durch die Wanderwartin nutzen. Das Vereinsmaterial erkennt Ihr an den blau, weiß, roten Klebebändern. Das nicht markierte Material ist Privateigentum und ist natürlich ohne Absprache mit dem Eigentümer tabu.

Die Vereinsschwimmwesten dürft Ihr ein Jahr lang und die Vereinspaddel zwei Jahre lang regelmäßig nutzen. Danach sind Eure eigenen Schwimmwesten und Paddel zu verwenden.

Mitglieder können für private, Vereins- oder Verbandsveranstaltungen ein Boot über das elektronische Fahrtenbuch reservieren. Bitte bei Reservierungsgrund eine Telefonnummer – möglichst mobil – für Rückfragen eintragen. Eine Reservierung für die Trainingszeiten ist nicht möglich. Der Transport der Boote erfolgt mit den eigenen Spanngurten.

An den Trainingszeiten z.Zt. montags und mittwochs, soll das Vereinsmaterial vollständig zur Verfügung stehen. Eine Ausleihe ist an diesen Tagen nicht möglich.

Bei Ausleihwünschen über eine Tagestour hinaus sprecht bitte vorher mit der Wanderwartin. Für Vereins- und Verbandsfahrten ist keine Absprache notwendig. Allerdings ist zu kontrollieren, ob das entsprechende Material verfügbar ist.

Wer die benötigten Fähigkeiten und Kenntnisse für Solofahrten auf einem Großgewässer nachgewiesen hat, darf auch außerhalb der Trainingszeiten alleine mit dem Vereinsmaterial paddeln. Auch hier gilt, dass eintägige Fahrten auf der Förde und auf der Schwentine ohne Absprache möglich sind und längere Touren oder andere Gewässer abgesprochen werden.

Nichtmitglieder dürfen das Vereinsmaterial außerhalb der Trainingszeiten nicht nutzen, es sei denn, dass ein Vereinsmitglied die volle Verantwortung für das Material und die Fahrtdurchführung übernimmt. Das betreuende Vereinsmitglied genügt dabei den Kriterien einer Solofahrt. Die Ausleihe für längere Touren – mehr als ein Tag – oder andere Gewässer müssen auch hier mit der Wanderwartin abgesprochen werden.

Missgeschicke mit dem Vereinsmaterial können passieren. Gebt bitte Mängel bei verschuldeten oder auch nur beobachteten Mängel umgehend der Wanderwartin Bescheid geben, damit sie behoben werden können.

Da sich jeder freut, wenn er/sie in ein sauberes, trockenes Boot einsteigen kann, bitte nach der Rückkehr zum Verein das Boot, die Spritzdecke, die Schwimmweste und das Paddel reinigen/abspülen. Die Schwimmwesten im Poloschuppen aufhängen. Lasst bitte die geschlossenen Cockpitdecken in die Luken der entsprechenden Boote, damit sie nicht in der Bootshalle verloren gehen.

Frohes Paddeln wünscht Euch
eure Wanderwartin
