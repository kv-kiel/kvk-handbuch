---
title	: Regeln und Anweisungen
weight	: 60
date                    : 2020-03-06T16:12:42+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

Hier findet ihr Regeln, die wir uns selbst gegeben haben.

{{% children depth="1" style="li" showhidden="true" %}}
