---
title                   : "Trailer"
tags                    : []
weight                  : 20
ressorts                : [Kanupolowartin, Wanderwartin]
date                    : 2020-03-06T16:15:27+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

![Trailer](../Trailer.jpg)

## Allgemein

Der Verein verfügt über einen Trailer, mit dem man eine große Menge Boote transportieren kann.

Verantwortlich für die Betreuung sind die
[Wanderwartin](/kvk-handbuch/04-vorstand/ressorts/wanderwartin/) und die [Kanupolowartin](/kvk-handbuch/04-vorstand/ressorts/kanupolowartin/).

Der Fahrzeugschein und der Schlüssel für das Deichselschloss liegen im Spind der 1.Vorsitzenden und müssen vor der Nutzung übergeben werden. Nach Nutzung sind sie wieder in den Briefschlitz am Spind zu werfen.

Der Trailer kann über das Elektronische Fahrtenbuch reserviert werden.

## Regeln

1. Der Fahrer haftet alleine und unteilbar für die Ladungssicherung und Einhaltung der gesetzlichen Bestimmungen.

2. Der Fahrer stellt die ordnungsgemäße Fahrbereitschaft mit voller Funktion der Leuchten, der richtigen Verlegung des Stromkabels, den richtigen Stromanschluss und das vollständige sichere Ankuppeln des Hängers sicher.

3. Der Fahrer sorgt für die Anbringung des Handbremsabrissdrahtes. (Kleine Stahlseilschlaufe wird um den Kugelkopf gelegt. Löst sich der Hänger, spannt dieses Seilstück die Handbremse, reißt sie fest, und geht dann erst flöten.)

4. Der Fahrer prüft den korrekten Luftdruck der Reifen, ihren einwandfreien Rundlauf und kontrolliert das Fahrwerk  und die Bremseinrichtung durch Sichtprüfung.

5. Der Fahrer ist verantwortlich für die Meldung aller Schäden an den Fachwart, die beim Gebrauch trotz aller Umsicht vielleicht doch mal auftreten könnten. Der Trailer muss sofort (nächste Fahrt) instandgesetzt werden. Nicht erst der nächste Entleiher darf am Freitagnachmittag unter Termindruck vor dem Turnier oder vor der Wanderfahrt feststellen, dass Mängel vorliegen.

6. Der Trailer muss ordnungsgemäß gesichert abgestellt werden. Zur Sicherung gegen Wegrollen darf die Handbremse nur kurzfristig benutzt werden, ansonsten muss man die Keile benutzen, da Bremsen gerne fest rosten.

7. Die Staukästen dienen der Aufnahme von leichtem und sperrigen Gepäck wie Helme, Paddel, Schwimmwesten, leichte weiche Sporttaschen usw. Punktbelastungen sind unbedingt zu vermeiden, es handelt sich um Leichtbaukästen. Bei richtiger Lastverteilung kann man sie gut nutzen, es sind immerhin ca. drei Kubikmeter Stauraum. Holzbänke und Tische können auch neben den Kästen direkt auf die Traversen festgezurrt werden. Da es ein Sportgeräteanhänger ist, darf er keinesfalls als Transporthänger für andere Zwecke benutzt werden, das wäre Steuerhinterziehung. (Grünes Nummernschild).- Verleih ist ausgeschlossen.

8. Vor der Abfahrt prüfen, ob lose Teile, wie z.B. nicht benötigte Gurte sich noch auf den Booten bzw. Trailer befinden.

Weitere Informationen:

Hier einige Daten: Gesamtgewicht bis 1300 kg, Höhe des Transportgestells muss unbedingt beachtet werden, Breite 2,10 Meter, Höhe ca. 3,2 m, langer Radstand erfordert viel Manöverraum in Kurven (immer nach Spiegel fahren, man braucht viel Platz), Gesamtgespannlänge bei Nutzung mit ausgezogenem Leuchtenträger (z.B. Zehner-Canadiertransport) zusammen mit Schulbus 13,50 Meter, Hängerschein notwendig bei jungen Fahrern, Höchstgeschwindigkeit mit Schulbus legal 100 km/h auf Autobahnen und Schnellfahrstrassen.

Der Vorstand, Kanu-Vereinigung Kiel
