---
title                   : "Schleifgeraet und Staubsauger"
tags                    : []
weight                  : 40
ressorts                : [Kanupolowartin, Wanderwartin]
date                    : 2020-10-19T10:36:15+02:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

{{% notice note %}}
Arbeiten mit Epoxy, Polyester oder sonstigen gesundheitsschädlichen Chemikalien dürfen nicht in der Bootshalle ausgeführt werden! Bitte dafür auf den gepflasterten Teil des Hofes gehen!
{{% /notice %}}

## Allgemein

Der Verein stellt den Mitgliedern ein Schleifgerät und einen zugehörigen Staubsauger für Arbeiten an privaten oder vereinseigene Booten, Paddeln oder sonstigem Sportgerät zur Verfügung:

- Schleifgerät: Exzenterschleifer Mirka DEROS 650CV
![Schleifgerät](../Schleifgeraet.png)
- Staubsauger: Nilfisk Multi II 22
![Staubsauger](../Staubsauger.png "Staubsauger")

Beide Geräte befinden sich in einem Funktionsspind in der Bootshalle.
Den Schlüssel für den Spind erhaltet ihr von der [Wanderwartin](/kvk-handbuch/04-vorstand/ressorts/wanderwartin/) oder der [Kanupolowartin](/kvk-handbuch/04-vorstand/ressorts/kanupolowartin/).

## Voraussetzungen

- Ihr benötigt für die Benutzung eigenes Schleifpapier (außer bei der Reparatur von Vereinsmaterial, dieses sprecht mit der Wander- bzw. Kanupolowartin ab)
- Das Schleifpapier muss einen Durchmesser von 125 mm haben (oder 150 mm) und mindestens 6 Löcher besitzen (im Baumarkt erhältlich)
- Die Beutel für den Stabsauger stellt der KVK. Den Staubsauger **immer** mit Beutel benutzen beim Schleifen. Vorher bitte kontrollieren, ob ein Beutel drin ist.
- Nur draußen über den Steinplatten schleifen, nicht auf dem Rasen oder in der Bootshalle und immer mit angeschlossenem und eingeschaltetem Staubsauger.

## Anleitung

![Schleifgerät](../Schleifgeraet-Bedienfeld.png)

- Staubsauger an dem Schleifgerät befestigen (notfalls mit Gewebeklebeband die Verbindung „unterstützen“ – dabei auf keinen Fall die Lüftungsschlitze von dem Schleifer zukleben!!!)
- Kontrollieren, ob ein Beutel im Staubsauger ist
- Schleifpapier auf den Schleifteller kletten / Löcher von Schleifteller und Schleifpapier bündig übereinander, damit darüber der entstehende Staub abgesaugt werden kann
- Es sind 2 verschieden große Schleifteller vorhanden: 125 mm und 150 mm Durchmesser - diese kann man nach Belieben und der Größe des Schleifpapiers austauschen
- Schleifgerät anschalten (grüne Lampe geht dabei an)
- Man kann mit dem + und dem – Symbol die Drehgeschwindigkeit des Schleifers einstellen 

**Los geht’s ;-)**