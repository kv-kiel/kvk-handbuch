---
title 	: "Vereinszeitung"
tags	: []
ressorts: [Pressewartin]
date                    : 2019-11-16T09:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
weight                  : 120
---

Natürlich erstellen wir auch eine Vereinszeitung. Sie trägt den Titel **Platsch** und wird von einem engagierten
[Redationsteam](mailto:platsch@kv-kiel.de )
erstellt.

Die Erstellung und alles was mit dem Prozess zusammenhängt, aus den virtuellen Inhalten letztlich ein papiernes Produkt zu machen, ist auf einer
[eigenen Webseite](https://baltic-mh.github.io/Platsch/)
beschrieben.

Diese Webseite ist entstanden, um das Wissen der ehemaligen Redakteure festzuhalten, zu einer Zeit, als sich die Redaktion neu aufgestellt und organisiert hat. Mittlerweile hat die neue Redaktion den Prozess problemlos im Griff, und der Inhalt der Webseite gibt nicht mehr unbedingt den aktuellen Stand wieder.

Was aber immer und unabhängig vom Redaktionsteam gilt, ist, dass die Vereinszeitung davon lebt, das Berichte über das Vereinsleben eingereicht werden, Bilder von Veranstaltungen oder Touren oder einfach Beiträge, die andere Mitglieder interessieren oder vielleicht auch nur amüsieren können. Die Redaktion ist unter dieser Email-Adresse zu erreichen:

[**platsch@kv-kiel.de**](mailto:platsch@kv-kiel.de )