---
title	: Verein
weight	: 10
pre		: "<b>1. </b>"
chapter : true
date                    : 2019-12-16T19:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

### Kapitel 1

# Verein

![Viele](Viele.jpg)

Alles, was man wissen will, wenn man im Verein ist.

{{% children depth="1" style="li" showhidden="true" %}}
