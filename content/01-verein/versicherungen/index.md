---
title	: Versicherungen
weight	: 120
tags	: []
ressorts: ["2. Vorsitzende"]
date                    : 2020-03-01T16:47:51+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

Der Verein hat eine Vielzahl von Versicherungen abgeschlossen.
Diese sind im Folgenden näher beschrieben.

## Sportversicherung

Siehe auch https://www.arag.de/vereinsversicherung/FAQ/

| Versicherungsgesellschaft | ARAG |
| ------------------------- | ---- |
| seit                      |      |

## Sportversicherung für Nichtmitglieder

| Versicherungsgesellschaft | ARAG |
| ------------------------- | ---- |
| seit                      |      |

## KFZ-Haftpflicht Anhänger

| Versicherungsgesellschaft | HUK-Coburg |
| ------------------------- | ---------- |
| seit                      | 12.06.2019 |

alte Versicherung GEKÜNDIGT (AXA zum 27.06.2019)

## KFZ-Zusatzversicherung

| Versicherungsgesellschaft | ARAG |
| ------------------------- | ---- |
| seit                      |      |

Umstellung auf aktuelle, bessere Konditionen ab 04.09.2019.

## Sach-Gebäudevers. Vereinsheim

| Versicherungsgesellschaft | Aachen Münchener |
| ------------------------- | ---------------- |
| seit                      |                  |

## Sach-Gebäudevers. Bootsschuppen

| Versicherungsgesellschaft | Aachen Münchener |
| ------------------------- | ---------------- |
| seit                      |                  |

## Gebäude-Haftpflicht

| Versicherungsgesellschaft                 | Provinzial              |
| ----------------------------------------- | ----------------------- |
| In 2019 AUFGEHOBEN wg. Doppelversicherung | enthalten in Sportvers. |

## Sach-Inhaltsvers. Boote

| Versicherungsgesellschaft | Aachen Münchener |
| ------------------------- | ---------------- |
| seit                      |                  |

## Sach-Inhaltsvers. sonstige Einrichtung

| Versicherungsgesellschaft | Aachen Münchener |
| ------------------------- | ---------------- |
| seit                      |                  |

## D&O und Vermögensschaden-Haftpflicht (Vorstands-Haftungs-Schutz)

| Versicherungsgesellschaft | ARAG       |
| ------------------------- | ---------- |
| seit                      | 01.10.2019 |

