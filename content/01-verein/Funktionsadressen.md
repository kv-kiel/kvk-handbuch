---
title                   : "Funktionsadressen"
tags                    : []
ressorts                : []
date                    : 2021-08-08T11:59:35+02:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
weight                  : 180
---

Es gibt für alle Vorstandsposten generische Mail-Adressen:

| Vorstandsamt       | Adresse                         | Alias                         |
| ------------------ | ------------------------------- | ----------------------------- |
| Erste Vorsitzende  | erster.vorsitzender@kv-kiel.de  | erste.vorsitzende@kv-kiel.de  |
| Zweite Vorsitzende | zweiter.vorsitzender@kv-kiel.de | zweite.vorsitzende@kv-kiel.de |
| Kassenwartin       | kassenwart@kv-kiel.de           |                               |
| Wanderwartin       | wanderwart@kv-kiel.de           |                               |
| Polowartin         | sportwart@kv-kiel.de            | polowart@kv-kiel.de           |
| Mitgliederwartin   | beitragswart@kv-kiel.de         | mitgliederwart@kv-kiel.de     |
| Jugendwartin       | jugendwart@kv-kiel.de           | jugendwartin@kv-kiel.de       |
| Schriftwartin      | schriftwart@kv-kiel.de          |                               |
| Pressewartin       | pressewart@kv-kiel.de           |                               |
| Bauausschuss       | bauausschuss@kv-kiel.de         |                               |
| Bootshallenwartin  | bootshallenwart@kv-kiel.de      |                               |
| Ältestenrat        | aeltestenrat@kv-kiel.de         |                               |
| Festwart           | festwart@kv-kiel.de             |                               |
| Platsch-Redaktion  | platsch@kv-kiel.de              |                               |

Daneben gibt es einige Vereinsfunktionen 

| Adresse                  | Bemerkung                                                                      |
| ------------------------ | ------------------------------------------------------------------------------ |
| info@kv-kiel.de          | Generische Mailadresse des Vereins (stattdessen mögichst spezifische angeben!) |
| kanue@kv-kiel.de         | Pächter des Vereinsrestaurants                                                 |
| kieler.woche@kv-kiel.de  | Verantwortliche für das Kieler-Woche-Turnier                                   |
| kieler-woche@kv-kiel.de  | Verantwortliche für das Kieler-Woche-Turnier                                   |
| kielerwoche@kv-kiel.de   | Verantwortliche für das Kieler-Woche-Turnier                                   |
| turniere@kv-kiel.de      | Verantwortliche für das Kieler-Woche-Turnier                                   |
| uebernachtung@kv-kiel.de | Verantwortliche für Übernachtungsfragen                                        |
| poloherren@kv-kiel.de    | Alias für Mailing-Liste kanupolo@kv-kiel.de                                    |
| polodamen@kv-kiel.de     | Alias für Mailing-Liste kanupolo@kv-kiel.de                                    |


Die Änderung von Funktionsadressen kann nur durch den System-Adminstrator vorgenommen werden.

Um Funktionsadressen zu ändern oder hinzuzufügen, muss man sich auf dem Server einloggen (SSH/Konsole)

Die Datei /etc/postfix/virtual
enthält eine Zuordnung der Adressen, die Datei nach wünschen anpassen.

Anschließend muss daraus die Datenbank Datei erstellt werden und postfix muss neu gestartet werden.

postmap /etc/postfix/virtual
systemctl restart postfix

Anschließend sollte man überprüfen, ob alles funktioniert und der Postfix Dienst wieder läuft.

systemct status postfix
