---
title 	: "Mitgliederversammlung"
tags	: [Veranstaltungen]
ressorts: ["1. Vorsitzende"]
date                    : 2019-11-15T12:35:22+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
weight                  : 110
---

Einmal im Jahr findet eine Mitgliederversammlung statt. §8 unserer Satzung legt dazu einiges fest:

> (2) Eine ordentliche Mitgliederversammlung (Jahreshauptversammlung) findet in jedem Jahr **bis zum 31.03.** statt.
>
> (4) Die Einberufung der Mitgliederversammlungen erfolgt durch den Gesamtvorstand. Sie geschieht in **schriftlicher Form**. Zwischen dem Tage der Einberufung (Einladung) und dem Termin der Versammlung muss eine Frist von mindestens **zehn** Tagen liegen.
>
> (9) Über Anträge, die nicht schon in der Tagesordnung verzeichnet sind, kann in der Mitgliederversammlung nur abgestimmt werden, wenn diese Anträge mindestens **fünf Tage vor der Versammlung schriftlich** bei dem Vorsitzenden des Vereins eingegangen sind. ...

Der Vorstand - und insbesondere die 1. Vorsitzende - freut sich immer besonders über eine rege Beteiligung!
