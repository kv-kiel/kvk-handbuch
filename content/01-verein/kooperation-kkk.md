---
title                   : "Kooperation KKK"
tags                    : []
ressorts                : ["1. Vorsitzende"]
date                    : 2020-04-28T11:00:27+02:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
weight                  : 200
---

Mit dem Nachbarverein
[Kieler-Kanu-Klub](http://www.kieler-kanu-klub.de/)
besteht eine enge Kooperation.

{{% notice warning %}}
Protokolle nach entsprechenden Information durchsehen!
{{% /notice %}}
