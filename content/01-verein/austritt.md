---
title 	: "Austritt"
tags	: []
ressorts: [Mitgliederwartin]
weight: 20
date                    : 2020-01-24T17:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---


![Abschied](../Abschied.jpg)

Du möchtest den Verein verlassen?

Das ist natürlich bedauerlich, aber manchmal verläuft das Leben in krummen Bahnen, die sich nicht weiter mit einer Mitgliedschaft vereinbaren lassen.

Ganz formal erfolgt ein Austritt folgendermaßen:

1. Du teilst uns deinen Austritt mit. Nach der Satzung ist die Schriftform vorgesehen, aber wir akzeptieren auch EMail. Allerdings kommt es in seltenen Fällen mal vor, dass eine EMail in irgendwelchen elektronischen Schlingpflanzen hängenbleibt. Solltest du also nach einer angemessenen Wartezeit keine Reaktion erhalten haben, frag einfach noch einmal nach.

2. Auf der darauf folgenden Vorstandssitzung wird dein Antrag verlesen.

3. Du erhältst eine Mitteilung der Mitgliederwartin über deinen Austritt. Darin steht insbesondere der Termin, zu dem dein Austritt wirksam wird.

4. Da eventuelle Arbeitsdienstersatzzahlungen immer nachträglich eingezogen werden, musst du unter Umständen auch nach dem Termin, zu dem dein Austritt wirksam wird, noch damit rechnen, dass der Verein Geld von deinem Konto einzieht.

5. Solltest du einen Bootshallenschlüssel erhalten haben, ist dieser spätestens zum Austrittstermin zurückzugeben. Das eingezogene Schlüsselpfand wird mit der abschließenden Abbuchung zurückerstattet.

Wir freuen uns trotzdem, wenn du uns auch nach deinem Austritt hin und wieder einmal besuchst und vielleicht an der einen oder anderen Veranstaltung teilnimmst.
