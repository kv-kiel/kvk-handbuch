---
title 	: "Eintritt"
tags	: []
ressorts: [Mitgliederwartin]
weight: 10
date                    : 2019-12-16T19:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

Du interessierst dich für eine Mitgliedschaft?

![Aufnahmeantrag](../Aufnahmeantrag.png)

In der Regel empfehlen wir, erst einmal ein gegenseitiges Kennenlernen.
Du kannst gerne zu einer unser Veranstaltungen kommen und dir ein Bild vom Verein und den Leuten machen, auf die du triffst, wenn du dich uns anschließt.

Wenn du uns dann kennengelernt hast und immer noch der Meinung bist, Mitglied werden zu wollen, freut uns das sehr, und der Prozess der Aufnahme kann beginnen.

Ganz formal sieht er wie folgt aus:

1. Die füllst einen Aufnahmeantrag aus und gibst ihn bei uns ab.

2. Auf der darauf folgenden Vorstandssitzung wird formal über deinen Antrag beschlossen.

3. Du erhältst eine Mitteilung der Mitgliederwartin über die Aufnahme in den Verein, zusammen mit einigen grundlegenden Informationen und einer Einladung, dich dem Vorstand auf seiner nächsten Sitzung vorzustellen. Das trägt in unserem kleinen Verein dazu bei, dass wir uns gegenseitig kennen und keine anonyme Menge werden, wie in manchem großen Verein.

4. Ab sofort hast du alle Rechte - aber auch alle Pflichten, die man so als Vereinsmitlied hat.

Das Aufnahmeformular kannst du von unserer
[Homepage herunterladen](https://kv-kiel.de/verein/downloads)

Damit die Mitgliederwartin dich problemlos erreichen kann, ist es ausgesprochen hilfreich, wenn du eine EMail-Adresse auf deinem Aufnahmeantrag angibst.
