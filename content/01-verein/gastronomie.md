---
title 	: "Gastronomie"
tags	: []
ressorts: []
date                    : 2019-12-19T19:19:30+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

![KaNÜ](../kanue15.jpg "Gastraum KaNÜ")

Der Gastraum unseres Vereinsheimes ist verpachtet. Mirko betreibt hier das
[KaNÜ](http://www.kanue.de/index.html),
das man für private oder geschäftliche Veranstaltungen buchen kann.
