---
title                   : "Mailinglisten"
tags                    : []
ressorts                : []
date                    : 2020-06-14T23:10:51+02:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
weight                  : 120
---

![Mailinglisten](../mailinglisten.png)
Die Kanu-Vereinigung Kiel stellt für die Kommunikation der Mitglieder untereinander mehrere Mailinglisten zur Verfügung.
Darüber erfolgen z.B. Benachrichtungen und Bekanntmachungen des Vorstands oder der Ressortleiter.
Vor allem werden die Listen aber von den Vereinsmitgliedern selbst genutzt, um sich untereinander zu verabreden oder sich Informationen zukommen zu lassen.

Für die Anmeldung müsst ihr auf
[dieser Seite](https://lists.kv-kiel.de/lists)
die Liste auswählen, die ihr abonnieren wollt.

Auf der Seite, die dann auftaucht, wählt ihr im Menue link den Punkt `Abonnieren` aus und gebt rechts eure Email-Adresse und euren Namen an.

![Mailingliste-abonnieren](../mailingliste-abonnieren.png)

Nach Anmeldung wird der Listenverantwortliche per E-Mail informiert. Er führt dann die endgültige Freischaltung durch.

Die Vorstandsliste kann man nicht abonnieren - man kann sich aber in ein Vorstandsamt wählen lassen, dann wird man automatisch in diese Liste aufgenommen ;-)

Zum Abmelden geht ihr ähnlich wie beim Anmelden vor - nur wählt ihr diesmal `Abbestellen` aus und gebt die Email-Adresse ein, an die ihr immer die Listenmails geschickt bekommt.

![Mailingliste-abbestellen](../mailingliste-abbestellen.png)

Ein Handbuch für Listeneigner und Moderatoren ist unter [https://kv-kiel.de/wws/help/admin](https://kv-kiel.de/wws/help/admin) zu finden.
