---
title                   : "Bootshallenschlüssel"
tags                    : [Bootshalle]
ressorts                : [Mitgliederwartin, "1. Vorsitzende", "2. Vorsitzende", Kassenwartin]
date                    : 2020-02-27T17:41:31+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
weight                  : 80
---

![Schlüssel](../Schluessel.png)

Mitglieder, die das 16. Lebensjahr vollendet haben, können formlos einen Schlüssel für die Bootshalle beantragen.

Die Schlüssel für die Bootshalle sind im Spind der Vorsitzenden deponiert.
Die Schlüsselausgabe erfolgt über den geschäftsführenden Vorstand oder die Kanupolowartin. Die Mitgliederwartin ist unverzüglich darüber zu informieren. Der Termin der Übergabe und die Höhe des Schlüsselpfandes wird im Mitgliederverwaltungsprogramm vermerkt.

Die Schlüsselrückgabe erfolgt ebenfalls über den geschäftsführenden Vorstand oder die Kanupolowartin. Die Mitgliederwartin ist unverzüglich darüber zu informieren. Die Rückgabe des Schlüssels wird im Mitgliederverwaltungsprogramm vermerkt.
Das eingezogene Schlüsselpfand wird zurücküberwiesen.

Bei Austritt ist der Bootshallenschlüssel zurückzugeben.
Das vom Mitglied eingezogene Schlüsselpfand mit der abschließenden Abbuchung zurückerstattet.
Voraussetzung ist, dass der ausgegebene Hallenschlüssel vor dem Termin der abschließenden Abbuchung zurückgegeben wird.
Bei einer späteren Rückgabe verfällt das Schlüsselpfand.
