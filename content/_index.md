---
title: "KVK-Handbuch"
date                    : 2019-12-16T19:05:46+01:00
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---

# KVK-Handbuch

## Herzlich willkommen bei der Kanu-Vereinigung Kiel!

![Verein](Verein.jpg)

So ein Verein - denkt man - funktioniert von selbst, und alle wissen, was zu tun ist.

Das gilt, solange die Verantwortlichen bereits eine geraume Weile in ihren Ämtern wirken und mit den Aufgaben vertraut sind. Aber so ein Verein ist auch ein hoch dynamisches System und Veränderung ist Programm. Ständig kommen neue Mitglieder hinzu, was schön ist, aber altverdiente gehen, was manchmal unvermeidlich ist. Mit jedem Mitglied, das geht, geht auch ein bisschen Wissen, das für den Verein mitunter sehr wertvoll ist.

Auch dass Vorstandsmitglieder aus ihrem Amt ausscheiden und durch neue ersetzt werden, ist notwendig und zuerst einmal ein Segen. Aber auch hier kann wertvolles Wissen verloren gehen.

Damit im Verein erarbeitetes Wissen nicht ausschließlich bei den Personen hängenbleibt, die es ursprünglich erworben haben, sondern allen zur Verfügung steht, damit auch Neueinsteiger und Amtsnachfolger davon profitieren können, haben wir diese Webseite zusammengestellt.
Sie soll über die Prozesse und Gegebenheiten informieren, die unseren Verein ausmachen und am Laufen halten.
Sie ist als lebendes Dokument gedacht, das wachsen und je nach Bedarf auf den aktuellen
Stand gebracht werden soll --- vielleicht ja eines Tages sogar von
dir!

Links in der Navigationsleiste sind vier Bereite aufgelistet, in die wir die Informationen gruppiert haben.

Unter
[Tags]({{< relref "/tags" >}})
findest du die Informationen nach Stichwörtern sortiert.
Manchmal betrifft ein bestimmter Artikel ja noch andere Bereiche, dann versuchen wir, über ein entsprechendes Stichwort die Verbindung herzustellen.

Unter
[Ressorts]({{< relref "/ressorts" >}})
findest du die Informationen nach den einzelnen Vorstandsämtern sortiert. Hier sind alle Artikel aufgeführt, die einen Bezug zu diesem Vorstandsamt haben.
Der Unterschied zum entsprechenden Eintrag für einen Vorstandsposten im Bereich
[Vorstand]({{< relref "04-vorstand/ressorts" >}})
ist, dass bei letzterem die einzelnen Ämter jeweils im Detail beschrieben sind.

Und zu guter Letzt gibt es noch das _Suchfeld_ mit dem du eine Volltextsuche über alle Artikel dieser kompletten Webseite durchführen kannst.

Wenn du trotzdem eine Information nicht findest oder sie dir zu spärlich -- oder gar falsch erscheint, würden wir uns über eine entsprechende Nachricht freuen.
Wir werden uns dann bemühen, die Seite zu aktualisieren.

Und - ach ja: Veränderung ist Programm hieß es oben. Das bezieht sich nätürlich auch auf die Besetzung der insgesamt 18 Vorstandsämter.
Mal ist ein Mann Vorsitzender - dann wieder eine Frau Kassenwartin ;-) Damit diese Seite nicht bei jedem Wechsel angepasst werden muss, sind alle Amtsbezeichnungen hierin einfach in der weiblichen Form angegeben.
Wem das komisch vorkommt, sollte sich fragen, ob es ihm (ihr?) auch komisch vorgekommen wäre, wenn konsequent die männliche Form gewählt worden wäre...