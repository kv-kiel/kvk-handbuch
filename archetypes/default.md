---
title                   : "{{ replace .Name "-" " " | title }}"
tags                    : []
ressorts                : []
date                    : {{ .Date }}
LastModifierDisplayName : "BalticPaddler"
LastModifierEmail       : "howto@kv-kiel.de"
---
